'''
Created on 02/02/2012

@author: Martin
'''
class Book:
    '''
    classdocs
    '''
    
    def __init__(self, title, author, subject, description, published, purchased, condition, price, location):
        '''
        Constructor
        '''
        self.title = title
        self.author = author
        self.subject = subject
        self.description = description
        self.published = published
        self.purchased = purchased
        self.condition = condition
        self.price = price
        self.location = location
        
