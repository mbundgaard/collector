#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 11/07/2012

@author: Martin R. Bundgaard
'''

class Video():
    
    video_id = 0
    title = ''
    writer = ''
    director = ''
    genre = ''
    isbn = ''
    location = ''
    medie = ''
    runtime = ''
    year = ''
    released = ''
    actors = ''
    plot = ''
    image = ''
    

    def __init__(self, video_id, title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image):
       
        
        self.video_id = video_id
        self.title = title
        self.writer = writer
        self.director = director
        self.genre = genre
        self.isbn = isbn
        self.location = location
        self.medie = medie
        self.runtime = runtime
        self.year = year
        self.released = released
        self.actors = actors
        self.plot = plot
        self.image = image