#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 12/08/2012

@author: Martin R. Bundgaard
'''

class Music_track():
    '''
    classdocs
    '''
    track_id = 0
    number = 0
    title = ''
    artist = ''

    def __init__(self, track_id, number, title, artist):
        '''
        Constructor
        '''
        self.track_id = track_id
        self.number = number
        self.title = title
        self.artist = artist