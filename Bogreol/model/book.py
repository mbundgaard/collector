#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 22/01/2012

@author: Martin
'''

class Book():
    '''
    classdocs
    '''
    

    book_id = 0
    title = u''
    author = u''
    genre = u''
    publisher = u''
    isbn = u''
    ddc = u''
    purchased = u''
    published = u''
    condition = u''
    location = u''
    edition = u''
    pages = u''
    currency = u''
    new_price = u''
    purchased_price = u''
    description = u''
    note = u''
    image = u''
    
    def __init__(self, book_id, title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image):
        '''
        Constructor
        '''
        self.book_id = book_id
        self.title = title
        self.author = author
        self.genre = genre
        self.publisher = publisher
        self.isbn = isbn
        self.ddc = ddc
        self.purchased = purchased
        self.published = published
        self.condition = condition
        self.location = location
        self.edition = edition
        self.pages = pages
        self.currency = currency
        self.new_price = new_price
        self.purchased_price = purchased_price
        self.description = description
        self.note = note 
        self.image = image
        