#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 26/03/2012

@author: Martin
'''

class Item:
    
    def __init__(self):
        self.title = None
        self.description = None
        self.published = None
        self.purchased = None
        self.condition = None
        self.purchased_price = None
        self.new_price = None
        self.location = None
        self.image = None
        
        

    def get_title(self):
        return self.title
    
    def get_description(self):
        return self.description
    
    def get_published(self):
        return self.published
    
    def get_purchased(self):
        return self.purchased
    
    def get_purchased_price(self):
        return self.purchased_price
    
    def get_new_price(self):
        return self.new_price;
    
    def get_location(self):
        return self.location
    
    def get_image(self):
        return self.image
        