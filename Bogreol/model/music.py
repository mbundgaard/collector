#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 12/08/2012

@author: Martin R. Bundgaard
'''

class Music():
    '''
    classdocs
    '''
    music_id = 0
    title = ''
    artist = ''
    isbn = ''
    medie = ''
    location = ''
    published = ''
    genre = ''
    play_time = ''
    number_of_tracks = ''
    image = ''
    tracks = []

    def __init__(self, music_id, title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image, tracks):
        '''
        Constructor
        '''
        self.music_id = music_id
        self.title = title
        self.artist = artist
        self.isbn = isbn
        self.medie = medie
        self.location = location
        self.published = published
        self.genre = genre
        self.play_time = play_time
        self.number_of_tracks = number_of_tracks
        self.image = image
        self.tracks = tracks
        
        