#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 29/01/2012

@author: Martin
'''

import os
cur_path = os.getcwd()
ignore_set = set(["__init__.py", "count_sourcelines.py"])

loclist = []

for pydir, _, pyfiles in os.walk(cur_path):
    for pyfile in pyfiles:
        if pyfile.endswith(".py") and pyfile not in ignore_set:
            totalpath = os.path.join(pydir, pyfile)
            loclist.append( ( len(open(totalpath, "r").read().splitlines()),
                               totalpath.split(cur_path)[1]) )

for linenumbercount, filename in loclist: 
    print "%05d lines in %s" % (linenumbercount, filename)

print "\nTotal: %s lines (%s)" %(sum([x[0] for x in loclist]), cur_path)

##newfile = 'newfile.txt'
##new = open(newfile, "w")
#run = True
#line = 0
#while run == True:
#    
#    s = open('Aksels_boeger.txt', "r").readlines()
##    print s[line]
#    e = s[line].split('/  /')
#    r = s[line].split('       ')
#
#
#    for l in r:
#        print l
#    
#    line = line + 1
#    
#    '''   
#    new.writelines(l)
#new.close()    
#'''