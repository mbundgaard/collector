#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 22/01/2012

@author: Martin
'''
from model.book import Book
from model.video import Video
from model.music import Music
from model.music_track import Music_track
from dao.dao import Dao
from sort_asc_desc import Sort_asc_desc
import requests, json, os, shutil
import ConfigParser


class Service:

    sort = ''
    sort_desc_asc = Sort_asc_desc()
    appdata_path = unicode(os.environ['APPDATA'] + '/Collector')

    def __init__(self):
        '''
        Constructor
        '''

    #===========================================================================
    # Book section
    #===========================================================================
    
    @staticmethod
    def create_new_book(book):
        Dao.create_book(book)

#    @staticmethod 
#    def get_books():
#        Dao.get_books() 
    
    @staticmethod
    def select(): 
        o = Dao.select('SELECT id, title, description, quality_rating, price FROM Books ORDER BY price')
        return o
    
    @staticmethod
    def getbook(item_id):
        if item_id != '':
            
            o = Dao.get_book(item_id)
        
            for data in o:
                book_id = data[0]
                title = data[1]
                author = data[2]
                genre = data[3]
                publisher = data[4]
                isbn = data[5]
                ddc = data[6]
                purchased = data[7]
                published = data[8]
                condition = data[9]
                location = data[10]
                edition = data[11]
                pages = data[12]
                currency = data[13]
                new_price = data[14]
                purchased_price = data[15]
                description = data[16]
                note = data[17]
            
                image = Service.get_image('Books', book_id)
            return Book(book_id, title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image)
        
    @staticmethod    
    def update_book(book):
        Dao.update_book(book)

    @staticmethod     
    def get_book_count():     
        count = Dao.get_book_count()
        return count     
    
    #===========================================================================
    # Video section
    #===========================================================================

    @staticmethod
    def create_new_video(video):
        Dao.create_video(video)     
         
    @staticmethod     
    def get_video(video_id):     
        video = Dao.get_video(video_id)
        
        for data in video:
            video_id = data[0]
            title = data[1]
            writer = data[2]
            director = data[3]
            genre = data[4]
            isbn = data[5]
            location = data[6]
            medie = data[7]
            runtime = data[8]
            year = data[9]
            released = data[10]
            actors = data[11]
            plot = data[12]
            image = Service.get_image('Videos', video_id)
            
        return Video(video_id, title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image)
     
    @staticmethod
    def update_video(video):
        Dao.update_video(video)
     
    @staticmethod     
    def get_video_count():     
        count = Dao.get_video_count()
        return count 

    #===========================================================================
    # Music sesction
    #===========================================================================
    
    @staticmethod
    def create_new_music(music):
        Dao.create_music(music)

    @staticmethod
    def get_music(music_id):

        music = Dao.get_music(music_id)
        
        for data in music:
            music_id = data[0]
            title = data[1]
            artist = data[2]
            isbn = data[3]
            medie = data[4]
            location = data[5]
            published = data[6]
            genre = data[7]
            play_time = data[8]
            number_of_tracks = data[9]
        image = Service.get_image('Music', music_id)
        tracks = Service.get_tracks(music_id)
        
        return Music(music_id, title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image, tracks)    

    @staticmethod         
    def get_tracks(music_id): 
        t = Dao.get_tracks(music_id)  
        
        tracks = []    
        for track_data in t:
            track_id = track_data[0]
            number = track_data[1]
            title = track_data[2]
            artist = track_data[3]
            track = Music_track(track_id, number, title, artist)
            tracks.append(track)
        
        return tracks
    
    @staticmethod
    def update_music(music):
        Dao.update_music(music)
        
    @staticmethod     
    def get_musik_count():     
        count = Dao.get_musik_count()
        return count 
    
    #============================================================================
    # Generic funtions
    #============================================================================
   
    @staticmethod
    def get_list(table, column, counter):
        result = None
        if table == 'Books':
            result = Dao.select('SELECT id, title, author, genre, published FROM ' + table + ' ORDER BY ' + 'LOWER(' + column + ')' + Service.sort_desc_asc.sort_ad(column, counter))

        if table == 'Videos':
            result = Dao.select('SELECT id, title, writer, genre, year FROM ' + table + ' ORDER BY ' + 'LOWER(' + column + ')' + Service.sort_desc_asc.sort_ad(column, counter))

        if table == 'Music':
            result = Dao.select('SELECT id, title, artist, genre, published FROM ' + table + ' ORDER BY ' + 'LOWER(' + column + ')' + Service.sort_desc_asc.sort_ad(column, counter))

        return result

    @staticmethod
    def get_image(table, item_id):
        o = Dao.get_image(table, item_id)
        return o
   
    @staticmethod
    def delete_item(item_type, item_ids):
        
        for ids in item_ids:
            if item_type == 'Music':
                Dao.delete_tracks(ids)
    
            Dao.delete(item_type, ids)
    #============================================================================
    # Config section
    #============================================================================

    @staticmethod
    def set_selected_tab(tab):
        config = ConfigParser.ConfigParser()
        if os.path.isdir(Service.appdata_path) is False:
            os.mkdir(Service.appdata_path)

        config_file = open(Service.appdata_path + '/config.ini', 'w+')
        if tab == 0:
            config.add_section('tab')
            config.set('tab', 'book_tab', True)
            config.set('tab', 'video_tab', False)
            config.set('tab', 'music_tab', False)

        elif tab == 1:
            config.add_section('tab')
            config.set('tab', 'book_tab', False)
            config.set('tab', 'video_tab', True)
            config.set('tab', 'music_tab', False)
            
        elif tab == 2:
            config.add_section('tab')
            config.set('tab', 'book_tab', False)
            config.set('tab', 'video_tab', False)
            config.set('tab', 'music_tab', True)    
        config.write(config_file)
        config_file.close()
      
    @staticmethod
    def get_bool_setting(section, setting):

        config = ConfigParser.ConfigParser()
        if os.path.exists(Service.appdata_path + '/config.ini'):
            config.read(Service.appdata_path + '/config.ini')
        else:
            config.read('config.ini')
        return config.getboolean(section, setting)
    
    @staticmethod
    def set_bool_setting(section, option, value):
        config = ConfigParser.ConfigParser()
        config_file = open(Service.appdata_path + '/config.ini', 'w+')
        
        config.add_section(section)
        config.set(section, option, value) 
        
        config(config_file)
        config_file.close()

    @staticmethod
    def get_setting(section, setting):

        return Service.config_section(section)[setting]

    @staticmethod    
    def config_section(section):
        config = ConfigParser.ConfigParser()
        if os.path.exists(Service.appdata_path + '/config.ini'):
            config.read(Service.appdata_path + '/config.ini')
        else:
            config.read('config.ini')
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
                if dict1[option] == -1:
                    print("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1  

    #===========================================================================
    # Suggestions
    #===========================================================================
        
    @staticmethod
    def get_book_currency_suggestions():
        return Dao.get_suggestions('Books', 'currency')
    
    @staticmethod
    def get_book_condition_suggestions():
        return Dao.get_suggestions('Books', 'condition')
    
    @staticmethod
    def get_book_genre_suggestions():
        return Dao.get_suggestions('Books', 'genre')
    
    @staticmethod
    def get_video_genre_suggestions():
        return Dao.get_suggestions('Videos', 'genre')
    
    @staticmethod
    def get_video_medie_suggestions():
        return Dao.get_suggestions('Videos', 'medie')
    
    @staticmethod
    def get_music_genre_suggestions():
        return Dao.get_suggestions('Music', 'genre')
    
    @staticmethod
    def get_music_medie_suggestions():
        return Dao.get_suggestions('Music', 'medie')

    @staticmethod
    def update_checker():

        url = "http://mrbsoft.dk/api/collector/updatecheck"
        data = json.dumps({'name': 'test', 'description': 'some test repo'})

        try:
            r = requests
            r.Timeout = 0.5
            result = r.post(url)

        except:
            result = requests.ConnectionError

        return result

    @staticmethod
    def get_current_version():
        return '0.1'

    @staticmethod
    def get_db_version():
        return Dao.get_version()

    #==========================================================================
    # Images
    #==========================================================================

    @staticmethod
    def get_default_book_image():
        return 'resources/default_images/book.png'

    @staticmethod
    def get_default_cd_image():
        return 'resources/default_images/cd.png'

    @staticmethod
    def get_default_video_image():
        return 'resources/default_images/video.png'

    @staticmethod
    def check_config_file():

        if os.path.isdir(Service.appdata_path) is False:
            os.mkdir(Service.appdata_path)
            shutil.copy2('config.ini', Service.appdata_path)

        else:
            config = ConfigParser.ConfigParser()
            config_file = open(Service.appdata_path + '/config.ini', 'w+')
            if config.read(Service.appdata_path + '\\config.ini') is None:
                shutil.copy2('config.ini', Service.appdata_path)

    @staticmethod
    def get_home_path():
        return Dao.home_path