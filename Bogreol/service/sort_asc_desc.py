#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 16/09/2012

@author: Martin R. Bundgaard
'''

class Sort_asc_desc():
    '''
    classdocs
    '''

    sort = 'ASC'
    sort_desc = False

    def __init__(self):
        '''
        Constructor
        '''

    def sort_ad(self, column, counter):
        if counter % 2 != 0:
            self.sort = 'DESC'
        else:
            self.sort = 'ASC'

        return self.sort


        # def sort_ad(self, column, counter):
    #
    #     if column != self.temp_column:
    #         self.sort = ' ASC'
    #         self.temp_column = column
    #         self.sort_desc = True
    #
    #     else:
    #
    #         if self.sort_desc == False:
    #             self.sort = ' DESC'
    #             self.sort_desc = True
    #
    #         elif self.sort_desc == True:
    #             self.sort = ' ASC'
    #             self.sort_desc = False
    #
    #     return self.sort
    #