#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 22/01/2012

@author: Martin
'''

import sqlite3, gui, os
import service.service
class Dao():

    '''
    classdocs
    '''
    current_version = 0.1

    temp_path = os.environ['TEMP']
    home_path = os.environ['USERPROFILE'] + '/Documents/Collector'

    if os.path.isdir(home_path) is False:
        os.mkdir(home_path)

    DATABASE = home_path +"/database.db"
    
    conn = sqlite3.connect(DATABASE)
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute('CREATE TABLE IF NOT EXISTS Books(id INTEGER PRIMARY KEY AUTOINCREMENT, title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image);')
    cursor.execute('CREATE TABLE IF NOT EXISTS Videos(id INTEGER PRIMARY KEY AUTOINCREMENT, title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image)')
    cursor.execute('CREATE TABLE IF NOT EXISTS Music(id INTEGER PRIMARY KEY AUTOINCREMENT, title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image)')
    cursor.execute('CREATE TABLE IF NOT EXISTS Tracks(id INTEGER PRIMARY KEY AUTOINCREMENT, number, title, artist, music_id, FOREIGN KEY (music_id) REFERENCES Music(id))')
    cursor.execute('CREATE TABLE IF NOT EXISTS Settings(id INTEGER PRIMARY KEY AUTOINCREMENT, dbversion)')

    cursor.execute('SELECT dbversion FROM Settings')
    exits = cursor.fetchone()
    # print exits
    if exits is None:
        cursor.execute("INSERT INTO Settings(dbversion) VALUES(" + unicode(current_version) + ")")
        conn.commit()
    else:
       pass
    
    def __init__(self):
        '''
        Constructor
        '''

    #===================================================================
    # Book section
    #===================================================================

    @staticmethod
    def create_book(book):
        try:
            if book.title == '':
                raise Exception( u'Der skal som minimum være en titel')
            if book.image is not None:
                with open(book.image, 'rb') as image:
                    binary_image = image.read()
                    Dao.cursor.execute('INSERT INTO Books(title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                                   (book.title, book.author, book.genre, book.publisher, book.isbn, book.ddc, book.purchased, book.published, book.condition, book.location, book.edition, book.pages, book.currency, book.new_price, book.purchased_price, book.description, book.note, sqlite3.Binary(binary_image)))
            else:
                Dao.cursor.execute('INSERT INTO Books(title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                                   (book.title, book.author, book.genre, book.publisher, book.isbn, book.ddc, book.purchased, book.published, book.condition, book.location, book.edition, book.pages, book.currency, book.new_price, book.purchased_price, book.description, book.note, None))

        except Exception, e:
            
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        else:
            Dao.conn.commit()
        
    @staticmethod
    def get_book(item_id):
        try:  
            if item_id == '':
                raise Exception(u'Vælg en bog først')
            
        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)

        else:
            book = Dao.cursor.execute('SELECT id, title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image FROM Books WHERE id = ' + item_id)
            return book
    
    @staticmethod
    def update_book(book):
        
        try:
            if book.book_id == None:
                raise Exception(u'Update Error')
            if book.image is not None:
                with open(book.image, 'rb') as image:
                    binary_image = image.read()
                    Dao.cursor.execute('UPDATE Books SET title=?, author=?, genre=?, publisher=?, isbn=?, ddc=?, purchased=?, published=?, condition=?, location=?, edition=?, pages=?, currency=?, new_price=?, purchased_price=?, description=?, note=?, image=? WHERE id =?',
                                       (book.title, book.author, book.genre, book.publisher, book.isbn, book.ddc ,book.purchased, book.published, book.condition, book.location, book.edition, book.pages, book.currency, book.new_price, book.purchased_price, book.description, book.note, sqlite3.Binary(binary_image), book.book_id))

            else:
                Dao.cursor.execute('UPDATE Books SET title=?, author=?, genre=?, publisher=?, isbn=?, ddc=?, purchased=?, published=?, condition=?, location=?, edition=?, pages=?, currency=?, new_price=?, purchased_price=?, description=?, note=?, image=? WHERE id =?',
                                   (book.title, book.author, book.genre, book.publisher, book.isbn, book.ddc ,book.purchased, book.published, book.condition, book.location, book.edition, book.pages, book.currency, book.new_price, book.purchased_price, book.description, book.note, None, book.book_id))

        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()
            
    @staticmethod    
    def get_book_count():
        Dao.cursor.execute('SELECT COUNT(*) FROM Books')
        
        for i in Dao.cursor:
            for j in i:
                return j
    
    #===========================================================================
    # Video section
    #===========================================================================
    
    @staticmethod
    def update_video(video):
        
        try:
            if video.video_id == None:
                raise Exception(u'Update Error')
            if video.image is not None:
                with open(video.image, 'rb') as image:
                    binary_image = image.read()
                    Dao.cursor.execute('UPDATE Videos SET title=?, writer=?, director=?, genre=?, isbn=?, location=?, medie=?, runtime=?, year=?, released=?, actors=?, plot=?, image=? WHERE id =?',
                                       (video.title, video.writer, video.director, video.genre, video.isbn, video.location, video.medie, video.runtime, video.year, video.released, video.actors, video.plot, sqlite3.Binary(binary_image), video.video_id))
            else:
                Dao.cursor.execute('UPDATE Videos SET title=?, writer=?, director=?, genre=?, isbn=?, location=?, medie=?, runtime=?, year=?, released=?, actors=?, plot=?, image=? WHERE id =?',
                                   (video.title, video.writer, video.director, video.genre, video.isbn, video.location, video.medie, video.runtime, video.year, video.released, video.actors, video.plot, None, video.video_id))

        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()
                    
   
    @staticmethod    
    def get_video_count():
        Dao.cursor.execute('SELECT COUNT(*) FROM Videos')
        
        for i in Dao.cursor:
            for j in i:
                return j
   
    @staticmethod
    def create_video(video):
        try:
            if video.title == '':
                raise Exception(u'Der skal som minimum være en titel')
            if video.image is not None:
                with open(video.image, 'rb') as image:
                    birary_image = image.read()
                    Dao.cursor.execute('INSERT INTO Videos(title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                                       (video.title, video.writer, video.director, video.genre, video.isbn,video. location, video.medie, video.runtime, video.year, video.released, video.actors, video.plot, sqlite3.Binary(birary_image)))
            else:
                Dao.cursor.execute('INSERT INTO Videos(title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                                   (video.title, video.writer, video.director, video.genre, video.isbn,video. location, video.medie, video.runtime, video.year, video.released, video.actors, video.plot, None))

        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()
            
            
    @staticmethod
    def get_video(video_id):
        video = Dao.cursor.execute('SELECT id, title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image FROM Videos WHERE id = ' + video_id)
        return video     
    
    #===========================================================================
    # Music section
    #===========================================================================
    
    @staticmethod
    def create_music(music):
        try:
            if music.title == '':
                raise Exception(u'Der skal som minimum være en titel')
            if music.image is not None:

                with open(music.image, 'rb') as image:
                    birary_image = image.read()
                    Dao.cursor.execute('INSERT INTO Music(title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image) VALUES(?,?,?,?,?,?,?,?,?,?)',
                                       (music.title, music.artist, music.isbn, music.medie, music.location, music.published, music.genre, music.play_time, music.number_of_tracks, sqlite3.Binary(birary_image)))

                    current_id = Dao.cursor.lastrowid
                    for data in music.tracks:

                        Dao.cursor.execute('INSERT INTO Tracks(number, title, artist, music_id) VALUES(?,?,?,?)',
                                           (data.number, data.title, data.artist, current_id,))
            else:
                Dao.cursor.execute('INSERT INTO Music(title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image) VALUES(?,?,?,?,?,?,?,?,?,?)',
                                   (music.title, music.artist, music.isbn, music.medie, music.location, music.published, music.genre, music.play_time, music.number_of_tracks, None))

                current_id = Dao.cursor.lastrowid
                for data in music.tracks:

                    Dao.cursor.execute('INSERT INTO Tracks(number, title, artist, music_id) VALUES(?,?,?,?)',
                                       (data.number, data.title, data.artist, current_id,))

        
        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()    
            
    @staticmethod        
    def get_music(music_id):
        return Dao.cursor.execute('SELECT id, title, artist,  isbn, medie, location, published, genre, play_time, number_of_tracks, image FROM Music WHERE id = ' + music_id)
   
    
    @staticmethod
    def get_tracks(music_id):
        return Dao.cursor.execute('SELECT id, number, title, artist, music_id FROM Tracks WHERE music_id = ' + unicode(music_id))
    
    @staticmethod
    def update_music(music):
        
        m_id = str(music.music_id)
        
        try:
            if music.music_id == None:
                raise Exception(u'Update Error')
            if music.image is not None:
                with open(music.image, 'rb') as image:
                    binary_image = image.read()
                    Dao.cursor.execute('UPDATE Music SET title=?, artist=?, isbn=?, medie=?, location=?, published=?, genre=?, play_time=?, number_of_tracks=?, image=? WHERE id =?',
                                       (music.title, music.artist, music.isbn, music.medie, music.location, music.published, music.genre, music.play_time, music.number_of_tracks, sqlite3.Binary(binary_image), unicode(music.music_id)))


                    Dao.cursor.execute('DELETE FROM Tracks WHERE id IN (SELECT id FROM Tracks WHERE music_id = ' + m_id +' )')

                    for data in music.tracks:

                        Dao.cursor.execute('INSERT INTO Tracks(number, title, artist, music_id) VALUES(?,?,?,?)',
                                           (data.number, data.title, data.artist, music.music_id,))
            else:
                Dao.cursor.execute('UPDATE Music SET title=?, artist=?, isbn=?, medie=?, location=?, published=?, genre=?, play_time=?, number_of_tracks=?, image=? WHERE id =?',
                                   (music.title, music.artist, music.isbn, music.medie, music.location, music.published, music.genre, music.play_time, music.number_of_tracks, sqlite3.Binary(binary_image), unicode(music.music_id)))


                Dao.cursor.execute('DELETE FROM Tracks WHERE id IN (SELECT id FROM Tracks WHERE music_id = ' + m_id +' )')

                for data in music.tracks:

                    Dao.cursor.execute('INSERT INTO Tracks(number, title, artist, music_id) VALUES(?,?,?,?)',
                                       (data.number, data.title, data.artist, music.music_id,))
#                
        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()
    
    @staticmethod    
    def get_musik_count():
        Dao.cursor.execute('SELECT COUNT(*) FROM Music')
        
        for i in Dao.cursor:
            for j in i:
                return j
    
    @staticmethod
    def delete_tracks(music_id):
        m_id = str(music_id)
        try:
            Dao.cursor.execute('DELETE FROM Tracks WHERE id IN (SELECT id FROM Tracks WHERE music_id = ' + m_id +' )')
         
        except Exception, e:
            gui.main_frame.Main_frame.message_box(e.message)
            Dao.conn.rollback()
          
        else:
            Dao.conn.commit()
            
        
    #===========================================================================
    # Generic section
    #===========================================================================
     
    @staticmethod
    def get_suggestions(table, column):
        
        suggestions = []
        Dao.cursor.execute('SELECT ' + column + ' FROM(SELECT ' + column + ', count(*) AS total FROM ' + table + ' GROUP BY ' + column +' ORDER BY total DESC)')
        
        for i in Dao.cursor:
            for j in i:
                if j != '':
                    suggestions.append(j)
        if suggestions == []:
            suggestions.append('')                               
        return suggestions  
     
    
    @staticmethod
    def get_image(table, item_id):
        image_path = Dao.temp_path + '\\.temp_image'
        image = Dao.cursor.execute('SELECT image FROM '+ table +' WHERE id = ' + str(item_id)).fetchone()
        if image[0] is not None:
            with open(image_path, "wb") as output_file:
                output_file.write(image[0])
                return image_path
        else:
            return None
    
    @staticmethod
    def select(sql):
        cursor = Dao.conn.cursor()
        cursor.execute(sql)
        records = cursor.fetchall()
        cursor.close
        return records
    
    
    @staticmethod
    def delete(item_type, item_ids):    
        try:
            Dao.cursor.execute('DELETE FROM ' + item_type + ' WHERE id = ' + item_ids)
            
        except:
            Dao.conn.rollback()
        
        else:
            Dao.conn.commit()
            
    @staticmethod
    def get_version():
        return Dao.cursor.execute('SELECT dbversion FROM Settings')
            
#    @staticmethod
#    def get_suggestions(table, column):
#        suggestions = []
#        Dao.cursor.execute('SELECT ' + column + ' FROM ' + table)
#        
#        for i in Dao.cursor:
#            for j in i:
#                if j not in suggestions:
#                    suggestions.append(j)
#                            
#        return suggestions