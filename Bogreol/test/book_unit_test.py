#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 06/09/2012

@author: Martin R. Bundgaard
'''
import unittest
from model.book import Book

class Test(unittest.TestCase):


    def testBoook(self):
        b = Book(1, 'title', 'author', 'genre', 'publisher', 'isbn', 'ddc', 'purchased', 'published', 'condition', 'location', 'edition', 'pages', 'currency', 'new_price', 'purchased_price', 'description', 'note', 'image')
        self.assertEqual(b.title, 'title')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

