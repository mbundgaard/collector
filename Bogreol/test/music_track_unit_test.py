#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 13/09/2012

@author: Martin R. Bundgaard
'''
import unittest
from model.music_track import Music_track


class Test(unittest.TestCase):


    def test_music_track(self):
        track = Music_track(1, 2, 'title', 'artist')
        
        self.assertEqual(track.track_id, 1)
        self.assertEqual(track.number, 2)
        self.assertEqual(track.title, 'title')
        self.assertEqual(track.artist, 'artist')
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_music_track']
    unittest.main()