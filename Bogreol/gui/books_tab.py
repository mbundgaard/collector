#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 23/01/2012

@author: Martin
'''
import wx
import sys
from service.service import Service
import wx.lib.mixins.listctrl as listmix
from service.sort_asc_desc import Sort_asc_desc


class Books_tab(wx.Panel):

    index = 0
    list_ctrl = ''
    image = Service.get_default_book_image()
    border_text = 7
    border_image = 4
    sort_counter = 0

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.splitter = wx.SplitterWindow(self)

        self.panel1 = wx.Panel(self.splitter, -1)
        self.panel2 = wx.Panel(self.splitter, -1)
        self.list_ctrl = wx.ListCtrl(self.panel1, style = wx.LC_REPORT | wx.BORDER_SUNKEN )
        self.list_ctrl.InsertColumn(0, u'ID', width = 0)
        self.list_ctrl.InsertColumn(1, u'Titel', width = 150)
        self.list_ctrl.InsertColumn(2, u'Forfatter',  width = 140)
        self.list_ctrl.InsertColumn(3, u'Genre', width = 90)
        self.list_ctrl.InsertColumn(4, u'Udgivet', width = 55)

        self.list_ctrl.Bind(wx.EVT_LIST_COL_CLICK, self.sort_data)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_SELECTED, self.get_items)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.Parent.Parent.Parent.list_right_click_menu)

        self.splitter.SplitVertically(self.panel1, self.panel2, sashPosition = 1)
        self.splitter.SetSashGravity(0.45)
        
        self.sizer_splitter = wx.BoxSizer(wx.VERTICAL)
        self.sizer_splitter.Add(self.splitter, 1, wx.ALL | wx.EXPAND)
        self.SetSizer(self.sizer_splitter)

        self.box_main_panel1 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.box1_panel1 = wx.BoxSizer(wx.VERTICAL)
        self.box1_panel1.Add(self.list_ctrl, 2, wx.ALL | wx.EXPAND)
        self.box_main_panel1.Add(self.box1_panel1, 1,  wx.ALL | wx.EXPAND, 7)
        
        self.box_main_panel2 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.gbsizer = wx.GridBagSizer(6,11)

        # Adds all the labels & textboxes -------------------------------------------------

        self.title = wx.StaticText(self.panel2, label = u'Titel')
        self.gbsizer.Add(self.title, pos = (0,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_title = wx.TextCtrl(self.panel2)
        self.tc_title.SetEditable(False)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.author = wx.StaticText(self.panel2, label = u'Forfatter')
        self.gbsizer.Add(self.author, pos = (1,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_author = wx.TextCtrl(self.panel2)
        self.tc_author.SetEditable(False)
        self.gbsizer.Add(self.tc_author, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.genre = wx.StaticText(self.panel2, label = u'Genre')
        self.gbsizer.Add(self.genre, pos = (2,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_genre = wx.TextCtrl(self.panel2)
        self.tc_genre.SetEditable(False)
        self.gbsizer.Add(self.tc_genre, pos = (2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.publisher = wx.StaticText(self.panel2, label = u'Forlag')
        self.gbsizer.Add(self.publisher, pos = (3,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_publisher= wx.TextCtrl(self.panel2)
        self.tc_publisher.SetEditable(False)
        self.gbsizer.Add(self.tc_publisher, pos = (3,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.isbn = wx.StaticText(self.panel2, label = u'ISBN')
        self.gbsizer.Add(self.isbn, pos = (4,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_isbn = wx.TextCtrl(self.panel2)
        self.tc_isbn.SetEditable(False)
        self.gbsizer.Add(self.tc_isbn, pos = (4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.ddc = wx.StaticText(self.panel2, label = u'DK5')
        self.gbsizer.Add(self.ddc, pos = (4,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_ddc = wx.TextCtrl(self.panel2)
        self.tc_ddc.SetEditable(False)
        self.gbsizer.Add(self.tc_ddc, pos = (4,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.purchased = wx.StaticText(self.panel2, label = u'Købt')
        self.gbsizer.Add(self.purchased, pos = (5,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_purchased = wx.TextCtrl(self.panel2)
        self.tc_purchased.SetEditable(False)
        self.gbsizer.Add(self.tc_purchased, pos = (5,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.published = wx.StaticText(self.panel2, label = u'Udgivet')
        self.gbsizer.Add(self.published, pos = (5,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_published = wx.TextCtrl(self.panel2)
        self.tc_published.SetEditable(False)
        self.gbsizer.Add(self.tc_published, pos = (5,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.condition = wx.StaticText(self.panel2, label = u'Tilstand')
        self.gbsizer.Add(self.condition, pos = (6,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_condition = wx.TextCtrl(self.panel2)
        self.tc_condition.SetEditable(False)
        self.gbsizer.Add(self.tc_condition, pos = (6,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.location = wx.StaticText(self.panel2, label = u'Lokation')
        self.gbsizer.Add(self.location, pos = (6,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_location = wx.TextCtrl(self.panel2)
        self.tc_location.SetEditable(False)
        self.gbsizer.Add(self.tc_location, pos = (6,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.edition = wx.StaticText(self.panel2, label = u'Udgave')
        self.gbsizer.Add(self.edition, pos = (7, 0), span = (1,1), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_edition = wx.TextCtrl(self.panel2)
        self.tc_edition.SetEditable(False)
        self.gbsizer.Add(self.tc_edition,pos = (7,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.pages= wx.StaticText(self.panel2, label = u'Antal sider')
        self.gbsizer.Add(self.pages, pos = (7, 2), span = (1,1), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_pages = wx.TextCtrl(self.panel2)
        self.tc_pages.SetEditable(False)
        self.gbsizer.Add(self.tc_pages,pos = (7,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)


        self.new_price = wx.StaticText(self.panel2, label = u'Ny pris')
        self.gbsizer.Add(self.new_price, pos = (8,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_new_price = wx.TextCtrl(self.panel2)
        self.tc_new_price.SetEditable(False)
        self.gbsizer.Add(self.tc_new_price, pos = (8,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.purchased_price = wx.StaticText(self.panel2, label = u'Købs pris')
        self.gbsizer.Add(self.purchased_price, pos = (8,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_purchased_price = wx.TextCtrl(self.panel2)
        self.tc_purchased_price.SetEditable(False)
        self.gbsizer.Add(self.tc_purchased_price, pos = (8,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.tc_currency = wx.TextCtrl(self.panel2)
        self.tc_currency.SetEditable(False)
        self.tc_currency.Value = u'DKK'
        self.gbsizer.Add(self.tc_currency, pos = (8,4), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.description = wx.StaticText(self.panel2, label = u'Bekrivelse')
        self.gbsizer.Add(self.description, pos = (9,0), flag = wx.TOP | wx.LEFT, border = self.border_text) 

        self.tc_description = wx.TextCtrl(self.panel2, style=wx.TE_MULTILINE)
        self.tc_description.SetEditable(False)
        self.gbsizer.Add(self.tc_description, pos = (9,1), span = (1, 4), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.note = wx.StaticText(self.panel2, label = u'Note')
        self.gbsizer.Add(self.note, pos = (10,0), flag = wx.TOP | wx.LEFT, border = self.border_text)

        self.tc_note  = wx.TextCtrl(self.panel2, style = wx.TE_MULTILINE)
        self.tc_note.SetEditable(False)
        self.gbsizer.Add(self.tc_note, pos = (10,1), span = (1, 4), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        # Sets the default image
        self.bitmap_image = self.convert_image(self.image)

        self.gbsizer.Add(self.bitmap_image, pos = (0,4), span = (8,1), flag = wx.TOP, border = self.border_image)
        # End ------------------------------------------------------------------

        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)

        self.gbsizer.AddGrowableRow(9)
        self.gbsizer.AddGrowableRow(10)

        self.box_main_panel2.Add(self.gbsizer, proportion = 1, flag = wx.ALL | wx.EXPAND, border = 10)

        self.panel1.SetSizer(self.box_main_panel1)
        self.panel2.SetSizer(self.box_main_panel2)

        # End ------------------------------------------------------------------

    #===========================================================================
    # Sets the image in the GUI        
    #===========================================================================
    def set_image(self, image):
       
        size_x = 200
        size_y = 275
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(size_x, size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        self.bitmap_image.SetBitmap(scaled_image)

    #===========================================================================
    # Converts the image to fit the frame    
    #===========================================================================
    def convert_image(self, image):
        size_x = 200
        size_y = 275
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(size_x,size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        finish_image = wx.StaticBitmap(self.panel2, -1, scaled_image)
        return finish_image 
       
    #===========================================================================
    # Updates the table data    
    #===========================================================================

    def update_list(self, column):

        items = Service.get_list('Books', column, self.sort_counter)

        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()
        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, str(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))

        self.list_ctrl.Update()   
        # self.clear_text_fields()
        self.list_ctrl.Thaw()

    def sort_list(self, column):
        self.sort_counter += 1
        items = Service.get_list('Books', column, self.sort_counter)

        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()
        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, str(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))

        self.list_ctrl.Update()
        # self.clear_text_fields()

        self.list_ctrl.Thaw()

    def get_items(self, event):
        book_id = self.list_ctrl.GetItem(event.m_itemIndex).GetText()

        book = Service.getbook(book_id)
        
        self.tc_title.SetValue(book.title)
        self.tc_author.SetValue(book.author)
        self.tc_genre.SetValue(book.genre)
        self.tc_publisher.SetValue(book.publisher)
        self.tc_isbn.SetValue(book.isbn)
        self.tc_ddc.SetValue(book.ddc)
        self.tc_purchased.SetValue(book.purchased)
        self.tc_published.SetValue(book.published)
        self.tc_condition.SetValue(book.condition)
        self.tc_location.SetValue(book.location)
        self.tc_edition.SetValue(book.edition)
        self.tc_pages.SetValue(book.pages)
        self.tc_currency.SetValue(book.currency)
        self.tc_new_price.SetValue(book.new_price)
        self.tc_purchased_price.SetValue(book.purchased_price)
        self.tc_description.SetValue(book.description)
        self.tc_note.SetValue(book.note)
        if book.image is None:
            self.set_image(Service.get_default_book_image())
        else:
            self.set_image(book.image)

    def clear_text_fields(self):
        self.tc_title.Clear()
        self.tc_author.Clear()
        self.tc_genre.Clear()
        self.tc_publisher.Clear()
        self.tc_isbn.Clear()
        self.tc_ddc.Clear()
        self.tc_purchased.Clear()
        self.tc_published.Clear()
        self.tc_condition.Clear()
        self.tc_location.Clear()
        self.tc_edition.Clear()
        self.tc_pages.Clear()
        self.tc_currency.Clear()
        self.tc_currency.Clear()
        self.tc_new_price.Clear()
        self.tc_purchased_price.Clear()
        self.tc_description.Clear()
        self.tc_note.Clear()

        self.set_image(self.image)

    def sort_data(self, event):
        if event.m_col == 1:
            self.sort_list('title')

        elif event.m_col == 2:
            self.sort_list('author')

        elif event.m_col == 3:
            self.sort_list('genre')

        elif event.m_col == 4:
            self.sort_list('published')