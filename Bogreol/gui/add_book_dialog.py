#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 07/03/2012

@author: Martin
'''
import wx
import os

from gui.drag_and_drop import Drag_and_drop
from service.service import Service
from model.book import Book

class Add_book_dialog(wx.Dialog):

    image = Service.get_default_book_image()
    border_text = 7
    border_image = 7
    
    size_x = 190
    size_y = 280
    
    currency_suggestions = []
    condition_suggestions = []
    genre_suggestions = []

    def __init__(self, parent, title):
        """

        :param parent:
        :param title:
        """
        wx.Dialog.__init__(self, parent, -1, title, size = (650, 600))
        self.SetBackgroundColour('white')
        
        vbox = wx.BoxSizer(wx.HORIZONTAL)        

        self.gbsizer = wx.GridBagSizer(6, 11)
        
        # Adds alls the labels -------------------------------------------------
        self.title = wx.StaticText(self, label = u'Title')
        self.gbsizer.Add(self.title, pos = (0,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_title = wx.TextCtrl(self)
        self.tc_title.SetEditable(True)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP |  wx.EXPAND, border = self.border_text)

        self.author = wx.StaticText(self, label = u'Forfatter')
        self.gbsizer.Add(self.author, pos = (1,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_author = wx.TextCtrl(self)
        self.tc_author.SetEditable(True)
        self.gbsizer.Add(self.tc_author, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.genre = wx.StaticText(self, label = u'Genre')
        self.gbsizer.Add(self.genre, pos = (2,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.cb_genre = wx.ComboBox(self)
        self.cb_genre.SetEditable(True)
        self.gbsizer.Add(self.cb_genre, pos = (2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.cb_genre.Bind(wx.EVT_KEY_UP, lambda event, func ="genre": self.match_suggestions(event, func))
        
        self.publisher = wx.StaticText(self, label = u'Forlag')
        self.gbsizer.Add(self.publisher, pos = (3,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_publisher= wx.TextCtrl(self)
        self.tc_publisher.SetEditable(True)
        self.gbsizer.Add(self.tc_publisher, pos = (3,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.isbn = wx.StaticText(self, label = u'ISBN')
        self.gbsizer.Add(self.isbn, pos = (4,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_isbn = wx.TextCtrl(self)
        self.tc_isbn.SetEditable(True)
        self.gbsizer.Add(self.tc_isbn, pos = (4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.ddc = wx.StaticText(self, label = u'DK5')
        self.gbsizer.Add(self.ddc, pos = (4,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_ddc = wx.TextCtrl(self)
        self.tc_ddc.SetEditable(True)
        self.gbsizer.Add(self.tc_ddc, pos = (4,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.purchased = wx.StaticText(self, label = u'Købt')
        self.gbsizer.Add(self.purchased, pos = (5,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_purchased = wx.TextCtrl(self)
        self.tc_purchased.SetEditable(True)
        self.gbsizer.Add(self.tc_purchased, pos = (5,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.published = wx.StaticText(self, label = u'Udgivet')
        self.gbsizer.Add(self.published, pos = (5,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_published = wx.TextCtrl(self)
        self.tc_published.SetEditable(True)
        self.gbsizer.Add(self.tc_published, pos = (5,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.condition = wx.StaticText(self, label = u'Tilstand')
        self.gbsizer.Add(self.condition, pos = (6,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.cb_condition = wx.ComboBox(self)
        self.cb_condition.SetEditable(True)
        self.gbsizer.Add(self.cb_condition, pos = (6,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.cb_condition.Bind(wx.EVT_KEY_UP, lambda event, func = 'condition': self.match_suggestions(event, func))

        self.location = wx.StaticText(self, label = u'Lokation')
        self.gbsizer.Add(self.location, pos = (6,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_location = wx.TextCtrl(self)
        self.tc_location.SetEditable(True)
        self.gbsizer.Add(self.tc_location, pos = (6,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.edition = wx.StaticText(self, label = u'Udgave')
        self.gbsizer.Add(self.edition, pos = (7, 0), span = (1,1), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_edition = wx.TextCtrl(self)
        self.tc_edition.SetEditable(True)
        self.gbsizer.Add(self.tc_edition,pos = (7,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.pages= wx.StaticText(self, label = u'Antal sider')
        self.gbsizer.Add(self.pages, pos = (7, 2), span = (1,1), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_pages = wx.TextCtrl(self)
        self.tc_pages.SetEditable(True)
        self.gbsizer.Add(self.tc_pages,pos = (7,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.new_price = wx.StaticText(self, label = u'Ny pris')
        self.gbsizer.Add(self.new_price, pos = (8,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_new_price = wx.TextCtrl(self)
        self.tc_new_price.SetEditable(True)
        self.gbsizer.Add(self.tc_new_price, pos = (8,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.purchased_price = wx.StaticText(self, label = u'Købs pris')
        self.gbsizer.Add(self.purchased_price, pos = (8,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_purchased_price = wx.TextCtrl(self)
        self.tc_purchased_price.SetEditable(True)
        self.gbsizer.Add(self.tc_purchased_price, pos = (8,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.cb_currency = wx.ComboBox(self)
        self.cb_currency.SetEditable(True)
        self.cb_currency.SetMinSize(wx.Size(80))
        self.gbsizer.Add(self.cb_currency, pos = (8,4), span = (1,1), flag = wx.TOP | wx.RIGHT | wx.EXPAND,  border = self.border_text)

        self.cb_currency.Bind(wx.EVT_KEY_UP, lambda event, func = 'currency': self.match_suggestions(event, func))

        self.description = wx.StaticText(self, label = u'Bekrivelse')
        self.gbsizer.Add(self.description, pos = (9,0), flag = wx.TOP | wx.LEFT, border = self.border_text) 

        self.tc_description = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.tc_description.SetEditable(True)
        self.gbsizer.Add(self.tc_description, pos = (9,1), span = (1, 4), flag = wx.TOP | wx.RIGHT | wx.EXPAND, border = 7)

        self.note = wx.StaticText(self, label = u'Note')
        self.gbsizer.Add(self.note, pos = (10,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_note  = wx.TextCtrl(self, style = wx.TE_MULTILINE)
        self.tc_note.SetEditable(True)
        self.gbsizer.Add(self.tc_note, pos = (10,1), span = (1, 4), flag = wx.TOP | wx.RIGHT | wx.EXPAND, border = 7)

        self.btn_add = wx.Button(self, label = u'Tilføj')
        self.gbsizer.Add(self.btn_add, pos = (11,1), span = (1, 1), flag = wx.TOP | wx.BOTTOM , border  = 7)
        self.btn_add.Bind(wx.EVT_BUTTON, self.create_book)
        
        self.btn_close = wx.Button(self, label =u'Luk')
        self.gbsizer.Add(self.btn_close, pos = (11, 0), span = (1, 1), flag = wx.ALL, border = 7)
        self.btn_close.Bind(wx.EVT_BUTTON, self.close_windows)
        # End ------------------------------------------------------------------
        
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        
        self.image_button = wx.BitmapButton(self, id=-1, bitmap = image, pos =(10,20), size = (self.size_x, self.size_y))
        self.image_button.Bind(wx.EVT_BUTTON, self.image_button_click)

        self.gbsizer.Add(self.image_button, pos = (0,4), span = (8,1), flag = wx.RIGHT | wx.TOP | wx.EXPAND, border = self.border_image)
        
        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)
        
        self.gbsizer.AddGrowableRow(9)
        self.gbsizer.AddGrowableRow(10)
        
        
        vbox.Add(self.gbsizer, 1, wx.EXPAND)
        self.SetSizer(vbox)
        
        dt = Drag_and_drop(self, self.image_button)
        self.image_button.SetDropTarget(dt)
        
        self.update()
        
    #===========================================================================
    # Closes the dialog window    
    #===========================================================================
        
    def close_windows(self, event):
        self.Destroy()
    
    #===========================================================================
    # Creates the item
    #===========================================================================
    def create_book(self, event):
        
        book_id = -1
        title = self.tc_title.GetValue()
        author = self.tc_author.GetValue()
        genre = self.cb_genre.GetValue()
        publisher = self.tc_publisher.GetValue()
        isbn = self.tc_isbn.GetValue()
        ddc = self.tc_ddc.GetValue()
        purchased = self.tc_purchased.GetValue()
        published = self.tc_published.GetValue()
        condition = self.cb_condition.GetValue()
        location = self.tc_location.GetValue()
        edition = self.tc_edition.GetValue()
        pages = self.tc_pages.GetValue()
        currency = self.cb_currency.GetValue()
        new_price = self.tc_new_price.GetValue()
        purchased_price = self.tc_purchased_price.GetValue()
        description = self.tc_description.GetValue()
        note = self.tc_note.GetValue()

        if self.image != Service.get_default_book_image():
            image = self.image
        else:
            image = None
        
        book = Book(book_id, title, author, genre, publisher, isbn, ddc, purchased, published, condition, location, edition, pages, currency, new_price, purchased_price, description, note, image)  
        
        Service.create_new_book(book)    
        
        self.Parent.update()
        
        
        if self.tc_title.Value != str(""):
            self.Parent.message_box(u"Din bog er tilføjet")
        
            self.update()
    def update(self):

        self.condition_suggestions = self.get_condition_suggestions()
        self.currency_suggestions = self.get_currency_suggestions()
        self.genre_suggestions = self.get_genre_suggestions()
    
        self.tc_title.Clear()
        self.tc_title.SetFocus()
        self.tc_author.Clear()
        self.cb_genre.SetItems(self.genre_suggestions)
        # self.cb_genre.SetValue(self.genre_suggestions[0])
        self.tc_publisher.Clear()
        self.tc_isbn.Clear()
        self.tc_ddc.Clear()
        self.tc_purchased.Clear()
        self.tc_published.Clear()
        self.cb_condition.SetItems(self.condition_suggestions)
        # self.cb_condition.SetValue(self.condition_suggestions[0])
        self.tc_location.Clear()
        self.tc_edition.Clear()
        self.tc_pages.Clear()
        self.cb_currency.SetItems(self.currency_suggestions)
        # self.cb_currency.SetValue(self.currency_suggestions[0])
        self.tc_new_price.Clear()
        self.tc_purchased_price.Clear()
        self.tc_description.Clear()
        self.tc_note.Clear()
        self.image_button.Refresh()   


    #===========================================================================
    # Create the button placeholder for the image    
    #===========================================================================
    def image_button_click(self, event):
        file_dialog = wx.FileDialog(self, message = u'vælg et billede', defaultDir = os.getcwd(), defaultFile = "", style = wx.OPEN)

        if file_dialog.ShowModal() == wx.ID_OK:
            self.convert_image(file_dialog.GetPath())    
                
    #===========================================================================
    # Resizes the image to fit the image placeholder   
    #===========================================================================
    def convert_image(self, filepath):
        self.image = filepath
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        self.image_button.SetBitmapLabel(image)

    def get_currency_suggestions(self):
        return Service.get_book_currency_suggestions()
    
    def get_condition_suggestions(self):
        return Service.get_book_condition_suggestions()
    
    def get_genre_suggestions(self):
        return Service.get_book_genre_suggestions() 
       

    def match_suggestions(self, event, func):
        if event.GetKeyCode() != wx.WXK_BACK and event.GetKeyCode() != wx.WXK_DELETE and event.GetKeyCode() != wx.WXK_SHIFT:
            if func == 'currency':
                value = self.cb_currency.GetValue().lower()
                
                for item in self.currency_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_currency.SetValue(item)
                        self.cb_currency.SetInsertionPoint(len(value))
                        self.cb_currency.SetMark(len(value), len(item))
                        break
            
            elif func == 'genre':
                value = self.cb_genre.GetValue().lower()
                
                for item in self.genre_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_genre.SetValue(item)
                        self.cb_genre.SetInsertionPoint(len(value))
                        self.cb_genre.SetMark(len(value), len(item))
                        break
                    
                    
            elif func == 'condition':
                value = self.cb_condition.GetValue().lower()
                
                for item in self.condition_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_condition.SetValue(item)
                        self.cb_condition.SetInsertionPoint(len(value))
                        self.cb_condition.SetMark(len(value), len(item))
                        break

        event.Skip()
                    