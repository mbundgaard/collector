#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 14/04/2012

@author: Martin
'''
import wx
import sys
from service.service import Service

class Musik_tab(wx.Panel):
    
    image = Service.get_default_cd_image()
    
    border_text = 7
    border_image = 7
    sort_counter = 0
    size_x = 200
    size_y = 200
    
    def __init__(self, parent):
        
        wx.Panel.__init__(self, parent)

        self.splitter = wx.SplitterWindow(self)
        
        self.panel1 = wx.Panel(self.splitter, -1)
        self.panel2 = wx.Panel(self.splitter, -1) 
                             
        self.list_ctrl = wx.ListCtrl(self.panel1, style = wx.LC_REPORT | wx.BORDER_SUNKEN )
        self.list_ctrl.InsertColumn(0, u'ID', width = 0)
        self.list_ctrl.InsertColumn(1, u'Titel', width = 170)
        self.list_ctrl.InsertColumn(2, u'Kunster',  width = 170)
        self.list_ctrl.InsertColumn(3, u'Genre', width = 90)
        self.list_ctrl.InsertColumn(4, u'Udgivet', width = 55)

        self.list_ctrl.Bind(wx.EVT_LIST_COL_CLICK, self.sort_data)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_SELECTED, self.get_items)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.Parent.Parent.Parent.list_right_click_menu)

        self.splitter.SplitVertically(self.panel1, self.panel2, sashPosition = 1)
        self.splitter.SetSashGravity(0.45)
#        self.splitter.SetMinimumPaneSize(self.Parent.Parent.Parent.splitter_size)
        
        self.sizer_splitter = wx.BoxSizer(wx.VERTICAL)
        self.sizer_splitter.Add(self.splitter, 1, wx.ALL | wx.EXPAND)
        self.SetSizer(self.sizer_splitter)
    
        self.box_main_panel1 = wx.BoxSizer(wx.HORIZONTAL)
       
        self.box1_panel1 = wx.BoxSizer(wx.VERTICAL)
        self.box1_panel1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND)
        self.box_main_panel1.Add(self.box1_panel1, 1,  wx.ALL | wx.EXPAND, 7)
        
        self.box_main_panel2 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.gbsizer = wx.GridBagSizer(6,10)

        #Adds all the labels --------------------------------------------------

        self.st_title = wx.StaticText(self.panel2, label = 'Titel')
        self.gbsizer.Add(self.st_title, pos = (0,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_title = wx.TextCtrl(self.panel2)
        self.tc_title.SetEditable(False)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        
        self.st_artist = wx.StaticText(self.panel2, label = 'Kunstner')
        self.gbsizer.Add(self.st_artist, pos = (1,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_artist = wx.TextCtrl(self.panel2)
        self.tc_artist.SetEditable(False)
        self.gbsizer.Add(self.tc_artist, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_isbn = wx.StaticText(self.panel2, label = u'ISBN')
        self.gbsizer.Add(self.st_isbn, pos = (2,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_isbn = wx.TextCtrl(self.panel2)
        self.tc_isbn.SetEditable(False)
        self.gbsizer.Add(self.tc_isbn, pos =(2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_medie = wx.StaticText(self.panel2, label = 'Medie')
        self.gbsizer.Add(self.st_medie, pos = (3,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_medie = wx.TextCtrl(self.panel2)
        self.tc_medie.SetEditable(False)
        self.gbsizer.Add(self.tc_medie, pos = (3,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        
        self.st_location = wx.StaticText(self.panel2, label = 'Lokation')
        self.gbsizer.Add(self.st_location, pos = (3,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_location = wx.TextCtrl(self.panel2)
        self.tc_location.SetEditable(False)
        self.gbsizer.Add(self.tc_location, pos = (3,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_published = wx.StaticText(self.panel2, label = 'Udgivet')
        self.gbsizer.Add(self.st_published, pos = (4,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_published = wx.TextCtrl(self.panel2)
        self.tc_published.SetEditable(False)
        self.gbsizer.Add(self.tc_published, pos = (4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_genre = wx.StaticText(self.panel2, label = 'Genre')
        self.gbsizer.Add(self.st_genre, pos = (4,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_genre = wx.TextCtrl(self.panel2)
        self.tc_genre.SetEditable(False)
        self.gbsizer.Add(self.tc_genre, pos = (4,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_total_time = wx.StaticText(self.panel2, label = 'Tid ialt')
        self.gbsizer.Add(self.st_total_time, pos = (5,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_total_time = wx.TextCtrl(self.panel2)
        self.tc_total_time.SetEditable(False)
        self.gbsizer.Add(self.tc_total_time, pos = (5,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_tracks = wx.StaticText(self.panel2, label = 'Antal numre')
        self.gbsizer.Add(self.st_tracks, pos = (5,2), flag = wx.TOP | wx.LEFT, border = self.border_text)

        self.tc_tracks = wx.TextCtrl(self.panel2)
        self.tc_tracks.SetEditable(False)
        self.gbsizer.Add(self.tc_tracks, pos=(5, 3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.tracks_list_ctrl = wx.ListCtrl(self.panel2, style=wx.LC_REPORT | wx.BORDER_SUNKEN | wx.LC_NO_SORT_HEADER)

        # self.tracks_list_ctrl.InsertColumn(0, u'ID', width=0)
        self.tracks_list_ctrl.InsertColumn(0, u'Nr.', width=100)
        self.tracks_list_ctrl.InsertColumn(1, u'Titel', width=200)
        self.tracks_list_ctrl.InsertColumn(2, u'Kunstner', width=200)
        self.gbsizer.Add(self.tracks_list_ctrl, pos=(6, 0), span=(1, 5), flag=wx.TOP | wx.EXPAND, border = self.border_text)

        # self.tracks_list_ctrl.Bind(wx.EVT_SIZE, self.track_list_ctrl_size)

        self.bitmap_image = self.convert_image(self.image)
        self.gbsizer.Add(self.bitmap_image, pos=(0,4), span=(6,1), flag=wx.TOP, border = 6)

        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)
        self.gbsizer.AddGrowableRow(6)
        
        self.box_main_panel2.Add(self.gbsizer, proportion=1, flag=wx.ALL | wx.EXPAND, border=10)

        self.panel1.SetSizer(self.box_main_panel1)
        self.panel2.SetSizer(self.box_main_panel2)

    #===========================================================================
    # Sets the image in the GUI        
    #===========================================================================
    def set_image(self, image):
       
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        self.bitmap_image.SetBitmap(scaled_image)    

    #===========================================================================
    # Converts the image to fit the frame    
    #===========================================================================
    def convert_image(self, image):
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)

        return wx.StaticBitmap(self.panel2, -1, scaled_image)
    
    def update_list(self, column):
        s = Service
        items = s.get_list('Music', column, self.sort_counter)
        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()

        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))
            
        self.list_ctrl.Update()
        self.list_ctrl.Thaw()

    def sort_list(self, column):
        self.sort_counter += 1
        s = Service
        items = s.get_list('Music', column, self.sort_counter)
        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()

        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))

        self.list_ctrl.Update()

        self.list_ctrl.Thaw()

    def get_items(self, event):
        music_id = self.list_ctrl.GetItem(event.m_itemIndex).GetText()

        self.tracks_list_ctrl.DeleteAllItems()
        music = Service.get_music(music_id)
        self.tc_title.SetValue(music.title)
        self.tc_artist.SetValue(music.artist)
        self.tc_isbn.SetValue(music.isbn)
        self.tc_medie.SetValue(music.medie)
        self.tc_location.SetValue(music.location)
        self.tc_genre.SetValue(music.genre)
        self.tc_total_time.SetValue(music.play_time)
        self.tc_published.SetValue(music.published)
        self.tc_tracks.SetValue(music.number_of_tracks)
        if music.image is None:
            self.set_image(Service.get_default_book_image())
        else:
            self.set_image(music.image)
        
        for data in music.tracks:
            index = self.tracks_list_ctrl.InsertStringItem(sys.maxint, unicode(data.track_id))
            # self.tracks_list_ctrl.SetStringItem(index, 0, unicode(data.track_id))
            self.tracks_list_ctrl.SetStringItem(index, 0, unicode(data.number))
            self.tracks_list_ctrl.SetStringItem(index, 1, unicode(data.title))
            self.tracks_list_ctrl.SetStringItem(index, 2, unicode(data.artist))
            self.tracks_list_ctrl.Update()

    def clear_text_fields(self):
        self.tc_title.Clear()
        self.tc_artist.Clear()
        self.tc_isbn.Clear()
        self.tc_medie.Clear()
        self.tc_location.Clear()
        self.tc_genre.Clear()
        self.tc_total_time.Clear()
        self.tc_published.Clear()
        self.tc_tracks.Clear()
        self.set_image(self.image)

        self.tracks_list_ctrl.DeleteAllItems()

    def track_list_ctrl_size(self, event):
        x = (self.tracks_list_ctrl.GetSize().width - 121) * 0.50

        self.tracks_list_ctrl.SetColumnWidth(0, 100)
        self.tracks_list_ctrl.SetColumnWidth(1, x)
        self.tracks_list_ctrl.SetColumnWidth(2, x)
        event.Skip()

    def sort_data(self, event):
        if event.m_col == 1:
            self.sort_list('title')

        elif event.m_col == 2:
            self.sort_list('artist')

        elif event.m_col == 3:
            self.sort_list('genre')

        elif event.m_col == 4:
            self.sort_list('published')