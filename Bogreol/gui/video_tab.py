#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 13/04/2012

@author: Martin
'''

import wx
import sys
from service.service import Service

class Video_tab(wx.Panel):
    
    list_ctrl = ''
    image = Service.get_default_video_image()
    sort_counter = 0
    
    def __init__(self, parent):
        
        wx.Panel.__init__(self, parent)
        
        self.splitter = wx.SplitterWindow(self)
        
        self.panel1 = wx.Panel(self.splitter, -1)
        self.panel2 = wx.Panel(self.splitter, -1) 
        
                             
        self.list_ctrl = wx.ListCtrl(self.panel1, style = wx.LC_REPORT | wx.BORDER_SUNKEN )
        self.list_ctrl.InsertColumn(0, u'ID', width = 0)
        self.list_ctrl.InsertColumn(1, u'Titel', width = 150)
        self.list_ctrl.InsertColumn(2, u'Forfatter',  width = 140)
        self.list_ctrl.InsertColumn(3, u'Genre', width = 90)
        self.list_ctrl.InsertColumn(4, u'Udgivet', width = 55)

        self.list_ctrl.Bind(wx.EVT_LIST_COL_CLICK, self.sort_data)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_SELECTED, self.get_items)
        self.list_ctrl.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.Parent.Parent.Parent.list_right_click_menu)

        self.splitter.SplitVertically(self.panel1, self.panel2, sashPosition = 1)
        self.splitter.SetSashGravity(0.45)
#        self.splitter.SetMinimumPaneSize(self.Parent.Parent.Parent.splitter_size)
        
        self.sizer_splitter = wx.BoxSizer(wx.VERTICAL)
        self.sizer_splitter.Add(self.splitter, 1, wx.ALL | wx.EXPAND)
        self.SetSizer(self.sizer_splitter)
    
        self.box_main_panel1 = wx.BoxSizer(wx.HORIZONTAL)
       
        self.box1_panel1 = wx.BoxSizer(wx.VERTICAL)
        self.box1_panel1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND)
        self.box_main_panel1.Add(self.box1_panel1, 1,  wx.ALL | wx.EXPAND, 7)
        
        self.box_main_panel2 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.gbsizer = wx.GridBagSizer(6,11)
        
        self.panel1.SetSizer(self.box_main_panel1)
        
        # Adds all the textboxes -----------------------------------------------
        self.st_title = wx.StaticText(self.panel2, label = u'Titel')
        self.gbsizer.Add(self.st_title, pos = (0,0) , flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_title = wx.TextCtrl(self.panel2)
        self.tc_title.SetEditable(False)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_writer = wx.StaticText(self.panel2, label = u'Forfatter')
        self.gbsizer.Add(self.st_writer, pos = (1,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_writer = wx.TextCtrl(self.panel2)
        self.tc_writer.SetEditable(False)
        self.gbsizer.Add(self.tc_writer, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_director = wx.StaticText(self.panel2, label = u'Instruktør')
        self.gbsizer.Add(self.st_director, pos = (2,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_director = wx.TextCtrl(self.panel2)
        self.tc_director.SetEditable(False)
        self.gbsizer.Add(self.tc_director, pos = (2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_genre = wx.StaticText(self.panel2, label = u'Genre')
        self.gbsizer.Add(self.st_genre, pos = (3,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_genre = wx.TextCtrl(self.panel2)
        self.tc_genre.SetEditable(False)
        self.gbsizer.Add(self.tc_genre, pos = (3,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_isbn = wx.StaticText(self.panel2, label = u'ISBN')
        self.gbsizer.Add(self.st_isbn, pos = (4,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_isbn = wx.TextCtrl(self.panel2)
        self.tc_isbn.SetEditable(False)
        self.gbsizer.Add(self.tc_isbn, pos =(4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        self.st_location = wx.StaticText(self.panel2, label = u'Lokation')
        self.gbsizer.Add(self.st_location, pos=(4,2), flag = wx.TOP | wx.EXPAND, border = 7)
        
        self.tc_location = wx.TextCtrl(self.panel2)
        self.tc_location.SetEditable(False)
        self.gbsizer.Add(self.tc_location, pos=(4,3), span=(1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        self.st_media = wx.StaticText(self.panel2, label = u'Medie')
        self.gbsizer.Add(self.st_media, pos = (5,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_media = wx.TextCtrl(self.panel2)
        self.tc_media.SetEditable(False)
        self.gbsizer.Add(self.tc_media, pos = (5,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_runtime = wx.StaticText(self.panel2, label = u'Spille tid')
        self.gbsizer.Add(self.st_runtime, pos = (5,2), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_runtime = wx.TextCtrl(self.panel2)
        self.tc_runtime.SetEditable(False)
        self.gbsizer.Add(self.tc_runtime, pos = (5,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_year = wx.StaticText(self.panel2, label = u'År')
        self.gbsizer.Add(self.st_year, pos = (6,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_year = wx.TextCtrl(self.panel2)
        self.tc_year.SetEditable(False)
        self.gbsizer.Add(self.tc_year, pos = (6,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_released = wx.StaticText(self.panel2, label = u'Udgivet')
        self.gbsizer.Add(self.st_released, pos = (6,2), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_released = wx.TextCtrl(self.panel2)
        self.tc_released.SetEditable(False)
        self.gbsizer.Add(self.tc_released, pos = (6,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_actors = wx.StaticText(self.panel2, label = u'Medvirkende')
        self.gbsizer.Add(self.st_actors, pos = (7,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_actors = wx.TextCtrl(self.panel2, wx.TE_MULTILINE)
        self.tc_actors.SetEditable(False)
        self.gbsizer.Add(self.tc_actors, pos = (7,1), span = (3,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_plot = wx.StaticText(self.panel2, label = u'Plot')
        self.gbsizer.Add(self.st_plot, pos = (10,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_plot = wx.TextCtrl(self.panel2, wx.TE_MULTILINE)
        self.tc_plot.SetEditable(False)
        self.gbsizer.Add(self.tc_plot, pos = (10,1), span = (1,4), flag = wx.TOP | wx.EXPAND, border = 7)
        
    
        #===============================================================================
        # attributes:
        # Title, Director, Writer, Actors, 
        # Plot, Poster, Runtime, Rating, Votes, 
        # Genre, Released, Year, Rated, ID (IMDb ID)
        #===============================================================================
        
        # End ------------------------------------------------------------------
        
        self.bitmap_image = self.convert_image(self.image)
        
        self.gbsizer.Add(self.bitmap_image, pos=(0,4), span=(8,1), flag=wx.TOP | wx.BOTTOM, border = 5 )        
        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)
        
        self.gbsizer.AddGrowableRow(10)
        
        self.box_main_panel2.Add(self.gbsizer, proportion = 1, flag = wx.ALL | wx.EXPAND, border = 10)
        
        self.panel1.SetSizer(self.box_main_panel1)
        self.panel2.SetSizer(self.box_main_panel2)
        
        
    #===========================================================================
    # Sets the image in the GUI        
    #===========================================================================
    def set_image(self, image):
       
        size_x = 200
        size_y = 300
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(size_x,size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        self.bitmap_image.SetBitmap(scaled_image)    
        
        
    #===========================================================================
    # Converts the image to fit the frame    
    #===========================================================================
    def convert_image(self, image):
        size_x = 200
        size_y = 300
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(size_x,size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        finish_image = wx.StaticBitmap(self.panel2, -1, scaled_image)
        
        return finish_image      
    
    #===========================================================================
    # Updates the table data    
    #===========================================================================
    def update_list(self, column):
        s = Service
        items = s.get_list('Videos', column, self.sort_counter)

        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()
        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, str(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))
        
        self.list_ctrl.Update()
        self.list_ctrl.Thaw()

    def sort_list(self, column):
        self.sort_counter += 1
        s = Service
        items = s.get_list('Videos', column, self.sort_counter)

        self.list_ctrl.Freeze()
        self.list_ctrl.DeleteAllItems()
        for data in items:
            index = self.list_ctrl.InsertStringItem(sys.maxint, str(data[0]))
            self.list_ctrl.SetStringItem(index, 0, unicode(data[0]))
            self.list_ctrl.SetStringItem(index, 1, unicode(data[1]))
            self.list_ctrl.SetStringItem(index, 2, unicode(data[2]))
            self.list_ctrl.SetStringItem(index, 3, unicode(data[3]))
            self.list_ctrl.SetStringItem(index, 4, unicode(data[4]))

        self.list_ctrl.Update()

        self.list_ctrl.Thaw()
    
    def get_items(self, event):

        video_id = self.list_ctrl.GetItem(event.m_itemIndex).GetText()
        video = Service.get_video(video_id)
        
        self.tc_title.SetValue(video.title)
        self.tc_writer.SetValue(video.writer)
        self.tc_director.SetValue(video.director)
        self.tc_genre.SetValue(video.genre)
        self.tc_isbn.SetValue(video.isbn)
        self.tc_location.SetValue(video.location)
        self.tc_media.SetValue(video.medie)
        self.tc_runtime.SetValue(video.runtime)
        self.tc_year.SetValue(video.year)
        self.tc_released.SetValue(video.released)
        self.tc_actors.SetValue(video.actors)
        self.tc_plot.SetValue(video.plot)

        if video.image is None:
            self.set_image(Service.get_default_book_image())
        else:
            self.set_image(video.image)

    def clear_text_fields(self):
        self.tc_title.Clear()
        self.tc_writer.Clear()
        self.tc_director.Clear()
        self.tc_genre.Clear()
        self.tc_isbn.Clear()
        self.tc_location.Clear()
        self.tc_media.Clear()
        self.tc_runtime.Clear()
        self.tc_year.Clear()
        self.tc_released.Clear()
        self.tc_actors.Clear()
        self.tc_plot.Clear()

        self.set_image(self.image)

    def sort_data(self, event):
        if event.m_col == 1:
            self.sort_list('title')

        elif event.m_col == 2:
            self.sort_list('writer')

        elif event.m_col == 3:
            self.sort_list('genre')

        elif event.m_col == 4:
            self.sort_list('released')