#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 28/08/2012

@author: Martin R. Bundgaard
'''

import wx
import os
import sys

from gui.drag_and_drop import Drag_and_drop
from model.music_track import Music_track

from model.music import Music
from service.service import Service

from gui.editableListCtrl import EditableListCtrl

class Edit_music_dialog(wx.Dialog):
    
    '''
    classdocs
    '''
    
    border_text = 7
    image = Service.get_default_cd_image()
    
    size_x = 200
    size_y = 200
    
    item_id = ''
    
    track_list_ctrl_index = 0;
    item_index = 0

    def __init__(self, parent, title, item_id):
        wx.Dialog.__init__(self, parent, -1, title, size = (700, 600))
        
        self.item_id = item_id
        
        self.SetBackgroundColour('white')
        
        self.box_main_panel1 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.gbsizer = wx.GridBagSizer(6,10)
        
        #Adds all the labels --------------------------------------------------
        self.st_title = wx.StaticText(self, label = 'Titel')
        self.gbsizer.Add(self.st_title, pos = (0,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_title = wx.TextCtrl(self)
        self.tc_title.SetEditable(True)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        
        self.st_artist = wx.StaticText(self, label = 'Kunstner')
        self.gbsizer.Add(self.st_artist, pos = (1,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_artist = wx.TextCtrl(self)
        self.tc_artist.SetEditable(True)
        self.gbsizer.Add(self.tc_artist, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_isbn = wx.StaticText(self, label = u'ISBN')
        self.gbsizer.Add(self.st_isbn, pos = (2,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_isbn = wx.TextCtrl(self)
        self.tc_isbn.SetEditable(True)
        self.gbsizer.Add(self.tc_isbn, pos =(2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        
        self.st_medie = wx.StaticText(self, label = 'Medie')
        self.gbsizer.Add(self.st_medie, pos = (3,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.cb_medie = wx.ComboBox(self)
        self.cb_medie.SetEditable(True)
        self.gbsizer.Add(self.cb_medie, pos = (3,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.cb_medie.Bind(wx.EVT_KEY_UP, lambda event, func = 'medie': self.match_suggestions(event, func))

        self.st_location = wx.StaticText(self, label = 'Lokation')
        self.gbsizer.Add(self.st_location, pos = (3,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_location = wx.TextCtrl(self)
        self.tc_location.SetEditable(True)
        self.gbsizer.Add(self.tc_location, pos = (3,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_published = wx.StaticText(self, label = 'Udgivet')
        self.gbsizer.Add(self.st_published, pos = (4,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_published = wx.TextCtrl(self)
        self.tc_published.SetEditable(True)
        self.gbsizer.Add(self.tc_published, pos = (4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_genre = wx.StaticText(self, label = 'Genre')
        self.gbsizer.Add(self.st_genre, pos = (4,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.cb_genre = wx.ComboBox(self)
        self.cb_genre.SetEditable(True)
        self.gbsizer.Add(self.cb_genre, pos = (4,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.cb_genre.Bind(wx.EVT_KEY_UP, lambda event, func = 'genre': self.match_suggestions(event, func))
        
        self.st_total_time = wx.StaticText(self, label = 'Tid ialt')
        self.gbsizer.Add(self.st_total_time, pos = (5,0), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_total_time = wx.TextCtrl(self)
        self.tc_total_time.SetEditable(True)
        self.gbsizer.Add(self.tc_total_time, pos = (5,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.st_tracks = wx.StaticText(self, label = 'Antal numre')
        self.gbsizer.Add(self.st_tracks, pos = (5,2), flag = wx.TOP | wx.LEFT, border = self.border_text)
        
        self.tc_tracks = wx.TextCtrl(self)
        self.tc_tracks.SetEditable(False)
        self.tc_tracks.Disable()
        self.gbsizer.Add(self.tc_tracks, pos = (5,3), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        
        self.tracks_list_ctrl = EditableListCtrl(self, style = wx.LC_REPORT | wx.LC_SINGLE_SEL)
        
        self.tracks_list_ctrl.InsertColumn(0, u'ID', width = 0)
        self.tracks_list_ctrl.InsertColumn(1, u'Nr.', width = 50)
        self.tracks_list_ctrl.InsertColumn(2, u'Title', width = 300)
        self.tracks_list_ctrl.InsertColumn(3, u'Kunstner', width = 300)
        
        self.gbsizer.Add(self.tracks_list_ctrl, pos = (6,0), span = (3,5), flag = wx.TOP | wx.EXPAND, border = self.border_text)

        self.btn_add_track = wx.Button(self, label = '+')
        self.gbsizer.Add(self.btn_add_track, pos = (9,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        self.btn_add_track.Bind(wx.EVT_BUTTON, self.add_track)
        
        self.btn_remove_track = wx.Button(self, label = '-')
        self.gbsizer.Add(self.btn_remove_track, pos = (9,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = self.border_text)
        self.btn_remove_track.Bind(wx.EVT_BUTTON, self.remove_track)

        self.btn_close = wx.Button(self, label = 'Luk')
        self.gbsizer.Add(self.btn_close, pos = (10,0), span = (1,1), flag = wx.TOP, border = self.border_text)
        self.btn_close.Bind(wx.EVT_BUTTON, self.close_windows)
        
        self.btn_add = wx.Button(self, label = 'Opdater')
        self.gbsizer.Add(self.btn_add, pos = (10,1), span = (1,1), flag = wx.TOP, border = self.border_text)
        self.btn_add.Bind(wx.EVT_BUTTON, self.update_music_item)
        # End ------------------------------------------------------------------
        
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        
        self.image_button = wx.BitmapButton(self, id=-1, bitmap = image, pos =(10,20), size = (self.size_x, self.size_y))
        self.image_button.Bind(wx.EVT_BUTTON, self.image_button_click)

        self.gbsizer.Add(self.image_button, pos = (0,4), span = (6,1), flag = wx.BOTTOM | wx.TOP, border = 5)
        
        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)
        self.gbsizer.AddGrowableRow(7)
        
        self.box_main_panel1.Add(self.gbsizer, proportion = 1, flag = wx.ALL | wx.EXPAND, border = 10)
    
        self.SetSizer(self.box_main_panel1)
        
        dt = Drag_and_drop(self, self.image_button)
        self.image_button.SetDropTarget(dt)
        
        self.get_items()
        
    #===========================================================================
    # Sets the image in the GUI        
    #===========================================================================
    def set_image(self, image):
       
        bitmap = wx.Bitmap(image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH)
        scaled_image = wx.BitmapFromImage(image)
        self.bitmap_image.SetBitmap(scaled_image)    
    
    
    def image_button_click(self, event):
        file_dialog = wx.FileDialog(self, message = u'vælg et billede', defaultDir = os.getcwd(), defaultFile = "", style = wx.OPEN)

        if file_dialog.ShowModal() == wx.ID_OK:
            self.convert_image(file_dialog.GetPath())    
        
    #===========================================================================
    # Converts the image to fit the frame    
    #===========================================================================
    def convert_image(self, filepath):
        self.image = filepath
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        self.image_button.SetBitmapLabel(image)
    
    
    def add_track(self, event):
        
        index = self.tracks_list_ctrl.GetItemCount()
        
        self.tracks_list_ctrl.InsertStringItem(index, u'')
        self.tracks_list_ctrl.SetStringItem(index, 0, str(index))
        self.tracks_list_ctrl.SetStringItem(index, 1, str(index +1))
        
        if index % 2:
                self.tracks_list_ctrl.SetItemBackgroundColour(index, 'Light Grey')
        else:
            self.tracks_list_ctrl.SetItemBackgroundColour(index, "Grey")
        
        
        self.tc_tracks.SetValue(str(self.tracks_list_ctrl.GetItemCount()))
#        s = []
#        index = 0
#        while index < self.tracks_list_ctrl.ItemCount:
#            print self.tracks_list_ctrl.GetItem(index, 2).GetText()
#            
#            index = index + 1
    
    def remove_track(self, event):
        item_count = self.tracks_list_ctrl.GetItemCount()
        
        if item_count > 0:
            
            self.tracks_list_ctrl.DeleteItem(item_count -1)
            self.tc_tracks.SetValue(str(self.tracks_list_ctrl.GetItemCount()))
        
        
            
    def update_music_item(self, event):
        
        i = 0
        tracks_list = []
        
        while i < self.tracks_list_ctrl.GetItemCount():
            track = Music_track(int(self.tracks_list_ctrl.GetItem(i, 0).GetText()), int(self.tracks_list_ctrl.GetItem(i, 1).GetText()), self.tracks_list_ctrl.GetItem(i, 2).GetText(), self.tracks_list_ctrl.GetItem(i, 3).GetText())
            tracks_list.append(track)
            i += 1
        
        music_id = self.item_id
        title = self.tc_title.GetValue()
        artist = self.tc_artist.GetValue()
        isbn = self.tc_isbn.GetValue()
        medie = self.cb_medie.GetValue()
        location = self.tc_location.GetValue()
        published = self.tc_published.GetValue()
        genre = self.cb_genre.GetValue()
        play_time = self.tc_total_time.GetValue()
        number_of_tracks = self.tc_tracks.GetValue()
        if self.image != Service.get_default_book_image():
            image = self.image
        else:
            image = None
        
        music = Music(int(music_id), title, artist, isbn, medie, location, published, genre, play_time, number_of_tracks, image, tracks_list)        
      
        Service.update_music(music)
        
        self.Parent.update()
        
        if self.tc_title.Value != str(""):
            self.Parent.message_box(u"Opdateret!")  
         
        self.close_windows(None)    
    
    def get_items(self):
        if self.item_id != '':
            
            self.medie_suggestions = self.get_medie_suggestions()
            self.genre_suggestions = self.get_genre_suggestions()
            
            music = Service.get_music(self.item_id)
            self.tc_artist.SetValue(music.artist)
            self.cb_genre.SetItems(self.genre_suggestions)
            self.cb_genre.SetValue(music.genre)
            self.tc_isbn.SetValue(music.isbn)
            self.tc_location.SetValue(music.location)
            self.cb_medie.SetItems(self.medie_suggestions)
            self.cb_medie.SetValue(music.medie)
            self.tc_published.SetValue(music.published)
            self.tc_title.SetValue(music.title)
            self.tc_total_time.SetValue(music.play_time)
            self.tc_tracks.SetValue(music.number_of_tracks)

            if music.image is None:
                self.convert_image(Service.get_default_book_image())
            else:
                self.convert_image(music.image)
            
            for data in music.tracks:
                index = self.tracks_list_ctrl.InsertStringItem(sys.maxint, unicode(data.track_id))
                self.tracks_list_ctrl.SetStringItem(index, 0, unicode(data.track_id))
                self.tracks_list_ctrl.SetStringItem(index, 1, unicode(data.number))
                self.tracks_list_ctrl.SetStringItem(index, 2, unicode(data.title))
                self.tracks_list_ctrl.SetStringItem(index, 3, unicode(data.artist))
            
            self.tracks_list_ctrl.Update()
    
    def close_windows(self, event):
        self.Destroy()

    def get_medie_suggestions(self):
        return Service.get_music_medie_suggestions()
    
    def get_genre_suggestions(self):
        return Service.get_music_genre_suggestions()
    

        
    def match_suggestions(self, event, func):
    
        if event.GetKeyCode() != wx.WXK_BACK and event.GetKeyCode() != wx.WXK_DELETE and event.GetKeyCode() != wx.WXK_SHIFT:
            if func == 'medie':    
                value = self.cb_medie.GetValue().lower()
                for item in self.medie_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_medie.SetValue(item)
                        self.cb_medie.SetInsertionPoint(len(value))
                        self.cb_medie.SetMark(len(value), len(item))
                        break
            
            elif func == 'genre':
                value = self.cb_genre.GetValue().lower()
                
                for item in self.genre_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_genre.SetValue(item)
                        self.cb_genre.SetInsertionPoint(len(value))
                        self.cb_genre.SetMark(len(value), len(item))
                        break