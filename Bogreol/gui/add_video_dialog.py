#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 10/07/2012

@author: Martin R. Bundgaard
'''

import wx
import os

from gui.drag_and_drop import Drag_and_drop
from service.service import Service
from model.video import Video

class Add_video_dialog(wx.Dialog):
    
    image = Service.get_default_video_image()
    border_text = 7
    border_image = 7
    
    size_x = 200
    size_y = 310
    
    genre_suggestions = []
    medie_suggestions = []

    def __init__(self, parent, title):
        wx.Dialog.__init__(self, parent, -1, title, size = (650, 600))
        self.SetBackgroundColour('white')
        
        vbox = wx.BoxSizer(wx.HORIZONTAL)        

        self.gbsizer = wx.GridBagSizer(6, 11)
        
        # Adds all the textboxes -----------------------------------------------
        self.st_title = wx.StaticText(self, label = u'Titel')
        self.gbsizer.Add(self.st_title, pos = (0,0) , flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_title = wx.TextCtrl(self)
        self.tc_title.SetEditable(True)
        self.gbsizer.Add(self.tc_title, pos = (0,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_writer = wx.StaticText(self, label = u'Forfatter')
        self.gbsizer.Add(self.st_writer, pos = (1,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_writer = wx.TextCtrl(self)
        self.tc_writer.SetEditable(True)
        self.gbsizer.Add(self.tc_writer, pos = (1,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_director = wx.StaticText(self, label = u'Instruktør')
        self.gbsizer.Add(self.st_director, pos = (2,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_director = wx.TextCtrl(self)
        self.tc_director.SetEditable(True)
        self.gbsizer.Add(self.tc_director, pos = (2,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_genre = wx.StaticText(self, label = u'Genre')
        self.gbsizer.Add(self.st_genre, pos = (3,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.cb_genre = wx.ComboBox(self)
        self.cb_genre.SetEditable(True)
        self.gbsizer.Add(self.cb_genre, pos = (3,1), span = (1,3), flag = wx.TOP | wx.EXPAND, border = 7)

        self.cb_genre.Bind(wx.EVT_KEY_UP, lambda event, func = 'genre': self.match_suggestions(event, func))
        
        self.st_isbn = wx.StaticText(self, label = u'ISBN')
        self.gbsizer.Add(self.st_isbn, pos = (4,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_isbn = wx.TextCtrl(self)
        self.tc_isbn.SetEditable(True)
        self.gbsizer.Add(self.tc_isbn, pos =(4,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        self.st_location = wx.StaticText(self, label = u'lokation')
        self.gbsizer.Add(self.st_location, pos=(4,2), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_location = wx.TextCtrl(self)
        self.tc_location.SetEditable(True)
        self.gbsizer.Add(self.tc_location, pos=(4,3), span=(1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_media = wx.StaticText(self, label = u'Medie')
        self.gbsizer.Add(self.st_media, pos = (5,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.cb_medie = wx.ComboBox(self)
        self.cb_medie.SetEditable(True)
        self.gbsizer.Add(self.cb_medie, pos = (5,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)

        self.cb_medie.Bind(wx.EVT_KEY_UP, lambda event, func = 'medie': self.match_suggestions(event, func))
       
        
        self.st_runtime = wx.StaticText(self, label = u'Spille tid')
        self.gbsizer.Add(self.st_runtime, pos = (5,2), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_runtime = wx.TextCtrl(self)
        self.tc_runtime.SetEditable(True)
        self.gbsizer.Add(self.tc_runtime, pos = (5,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_year = wx.StaticText(self, label = u'År')
        self.gbsizer.Add(self.st_year, pos = (6,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_year = wx.TextCtrl(self)
        self.tc_year.SetEditable(True)
        self.gbsizer.Add(self.tc_year, pos = (6,1), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_released = wx.StaticText(self, label = u'Udgivet')
        self.gbsizer.Add(self.st_released, pos = (6,2), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_released = wx.TextCtrl(self)
        self.tc_released.SetEditable(True)
        self.gbsizer.Add(self.tc_released, pos = (6,3), span = (1,1), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_actors = wx.StaticText(self, label = u'Medvirkende')
        self.gbsizer.Add(self.st_actors, pos = (7,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_actors = wx.TextCtrl(self, style = wx.TE_MULTILINE)
        self.tc_actors.SetEditable(True)
        self.gbsizer.Add(self.tc_actors, pos = (7,1), span = (3,3), flag = wx.TOP | wx.EXPAND, border = 7)
        
        
        self.st_plot = wx.StaticText(self, label = u'Plot')
        self.gbsizer.Add(self.st_plot, pos = (10,0), flag = wx.TOP | wx.LEFT, border = 7)
        
        self.tc_plot = wx.TextCtrl(self, style = wx.TE_MULTILINE)
        self.tc_plot.SetEditable(True)
        self.gbsizer.Add(self.tc_plot, pos = (10,1), span = (1,4), flag = wx.TOP | wx.EXPAND, border = 7)
        
    
        self.btn_add = wx.Button(self, label = u'Tilføj')
        self.gbsizer.Add(self.btn_add, pos = (11,1), span = (1, 1), flag = wx.TOP | wx.BOTTOM , border  = 7)
        self.btn_add.Bind(wx.EVT_BUTTON, self.create_book)
        
        self.btn_close = wx.Button(self, label =u'Luk')
        self.gbsizer.Add(self.btn_close, pos = (11, 0), span = (1, 1), flag = wx.ALL, border = 7)
        self.btn_close.Bind(wx.EVT_BUTTON, self.close_windows)
        # End ------------------------------------------------------------------
        
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        
        self.image_button = wx.BitmapButton(self, id=-1, bitmap = image, pos =(10,20), size = (self.size_x, self.size_y))
        self.image_button.Bind(wx.EVT_BUTTON, self.image_button_click)

        self.gbsizer.Add(self.image_button, pos = (0,4), span = (10,1), flag = wx.RIGHT | wx.TOP | wx.EXPAND, border = self.border_image)
        
        self.gbsizer.AddGrowableCol(1)
        self.gbsizer.AddGrowableCol(3)
        
#       #self.gbsizer.AddGrowableRow(9)
        self.gbsizer.AddGrowableRow(10)
        
        
        vbox.Add(self.gbsizer, 1, wx.EXPAND)
        self.SetSizer(vbox)
        
        dt = Drag_and_drop(self, self.image_button)
        self.image_button.SetDropTarget(dt)
        
        self.update()
        
    #===========================================================================
    # Closes the dialog window    
    #===========================================================================
        
    def close_windows(self, event):
        self.Destroy()
    
    #===========================================================================
    # Creates the item
    #===========================================================================
    def create_book(self, event):
        
        video_id = -1
        title = self.tc_title.GetValue()
        writer = self.tc_writer.GetValue()
        director = self.tc_director.GetValue()
        genre = self.cb_genre.GetValue()
        isbn = self.tc_isbn.GetValue()
        location = self.tc_location.GetValue()
        medie = self.cb_medie.GetValue()
        runtime = self.tc_runtime.GetValue()
        year = self.tc_year.GetValue()
        released = self.tc_released.GetValue()
        actors = self.tc_actors.GetValue()
        plot = self.tc_plot.GetValue()
        if self.image != Service.get_default_book_image():
            image = self.image
        else:
            image = None
        
        video = Video(video_id, title, writer, director, genre, isbn, location, medie, runtime, year, released, actors, plot, image)  
        
        Service.create_new_video(video)    
        
        self.Parent.update()
        
        if self.tc_title.Value != str(""):
            self.Parent.message_box(u"Din film er tilføjet")
        
            self.update()    
    #===========================================================================
    # Create the button placeholder for the image    
    #===========================================================================
    def image_button_click(self, event):
        file_dialog = wx.FileDialog(self, message = u'vælg et billede', defaultDir = os.getcwd(), defaultFile = "", style = wx.OPEN)

        if file_dialog.ShowModal() == wx.ID_OK:
            self.convert_image(file_dialog.GetPath())    
                
    #===========================================================================
    # Resizes the image to fit the image placeholder   
    #===========================================================================
    def convert_image(self, filepath):
        self.image = filepath
        bitmap = wx.Bitmap(self.image)
        raw_image = wx.ImageFromBitmap(bitmap)
        image = raw_image.Scale(self.size_x, self.size_y, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        self.image_button.SetBitmapLabel(image)
        
    def update(self):
        
        self.genre_suggestions = self.get_genre_suggestions()
        self.medie_suggestions = self.get_medie_suggestions()
        
        self.tc_title.Clear()
        self.tc_title.SetFocus()
        self.tc_writer.Clear()
        self.tc_director.Clear()
        self.cb_genre.SetItems(self.genre_suggestions)
        # self.cb_genre.SetValue(self.genre_suggestions[0])
        self.tc_isbn.Clear()
        self.tc_location.Clear()
        self.cb_medie.SetItems(self.medie_suggestions)
        # self.cb_medie.SetValue(self.medie_suggestions[0])
        self.tc_runtime.Clear()
        self.tc_year.Clear()
        self.tc_released.Clear()
        self.tc_actors.Clear()
        self.tc_plot.Clear()
        self.image_button.Refresh()

    def get_genre_suggestions(self):
        return Service.get_video_genre_suggestions()
    
    def get_medie_suggestions(self):
        return Service.get_video_medie_suggestions()
    
    def match_suggestions(self, event, func):
        
        if event.GetKeyCode() != wx.WXK_BACK and event.GetKeyCode() != wx.WXK_DELETE and event.GetKeyCode() != wx.WXK_DELETE and event.GetKeyCode() != wx.WXK_TAB:
            if func == 'medie':
                value = self.cb_medie.GetValue().lower()
                for item in self.medie_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_medie.SetValue(item)
                        self.cb_medie.SetInsertionPoint(len(value))
                        self.cb_medie.SetMark(len(value), len(item))
                        break
            
            elif func == 'genre':
                value = self.cb_genre.GetValue().lower()
                
                for item in self.genre_suggestions:
                    if value == item[:len(value)].lower():
                        self.cb_genre.SetValue(item)
                        self.cb_genre.SetInsertionPoint(len(value))
                        self.cb_genre.SetMark(len(value), len(item))
                        break
        event.Skip()