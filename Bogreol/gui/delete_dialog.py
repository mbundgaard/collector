#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 08/05/2012

@author: martin
'''

import wx

class Deletedialog(wx.Frame):
    def __init__(self):

        def delete_dialog(self, event):
            text = 'Er du sikker på du vil slette?'
        
            deletedialog =  wx.MessageDialog(self, text, u'Info', wx.YES_NO | wx.ICON_QUESTION)
            result = deletedialog.ShowModal()
        
            if result == wx.ID_YES:
                self.delete_book()
        
            if result == wx.ID_NO:
                deletedialog.Destroy()