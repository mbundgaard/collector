#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 28/08/2012

@author: Martin R. Bundgaard
'''
import wx
import wx.lib.mixins.listctrl as listmix

class EditableListCtrl(wx.ListCtrl, listmix.TextEditMixin):
    ''' TextEditMixin allows any column to be edited. '''
    #----------------------------------------------------------------------
    def __init__(self, parent, ID=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=None):
        """Constructor"""
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.TextEditMixin.__init__(self)