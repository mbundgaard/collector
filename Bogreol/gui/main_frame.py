#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 23/01/2012

@author: Martin
'''
import os
import requests
import wx
import gui.books_tab as books
from gui.add_book_dialog import Add_book_dialog
from gui.add_video_dialog import Add_video_dialog
from gui.add_music_dialog import Add_music_dialog
from gui.edit_book_dialog import Edit_book_dialog
from gui.edit_video_dialog import Edit_video_dialog
from gui.edit_music_dialog import Edit_music_dialog
from gui.video_tab import Video_tab
from gui.musik_tab import Musik_tab
from service.service import Service


class Main_frame(wx.Frame):

    window_size = (1100, 620)
    splitter_size = 0
    image = Service.get_default_book_image()
    book_id = None
    book_tab_selected = False
    video_tab_selected = False
    music_tab_selected = False
    nb_tab_name = u''
    selected_item_ids = []

    def __init__(self):

        wx.Frame.__init__(self, None, title=u"Collector", size=self.window_size)

        self.Bind(wx.EVT_END_SESSION, self.on_quit)
        self.Bind(wx.EVT_QUERY_END_SESSION, self.on_quit)

        # Creates menubar ------------------------------------------------------
        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        new_submenu = wx.Menu()
        edit_menu = wx.Menu()
        edit_submenu = wx.Menu()
#        view_menu = wx.Menu()
        help_menu = wx.Menu()

        # Create menu item -----------------------------------------------------
        
        file_item_add_book = new_submenu.Append(wx.ID_ANY, u'Bog\tCtrl+1', kind = wx.ITEM_NORMAL)
        self.Bind(wx.EVT_MENU, lambda event, item = 0: self.add_dialog(event, item), file_item_add_book, wx.ID_ANY)

        file_item_add_video = new_submenu.Append(wx.ID_ANY, u'Video\tCtrl+2', kind = wx.ITEM_NORMAL)
        self.Bind(wx.EVT_MENU, lambda event, item = 1: self.add_dialog(event, item), file_item_add_video, wx.ID_ANY)

        file_item_add_musik = new_submenu.Append(wx.ID_ANY, u'Musik\tCtrl+3', kind = wx.ITEM_NORMAL)
        self.Bind(wx.EVT_MENU, lambda event, item = 2: self.add_dialog(event, item), file_item_add_musik, wx.ID_ANY)

        edit_item_edit_book = edit_menu.Append(wx.ID_ANY, u'Rediger valgte\tCtrl+E', kind = wx.ITEM_NORMAL)
        self.Bind(wx.EVT_MENU, lambda event: self.edit_item(event), edit_item_edit_book, wx.ID_ANY)

        edit_item_edit_book = edit_menu.Append(wx.ID_ANY, u'Slet valgte\tCtrl+D', kind = wx.ITEM_NORMAL)
        self.Bind(wx.EVT_MENU, lambda event: self.delete_dialog(event), edit_item_edit_book, wx.ID_ANY)
        
        file_menu.AppendMenu(wx.ID_ANY, u'Ny', new_submenu)

        file_item_quit = file_menu.Append(wx.ID_EXIT, u'Afslut\tAlt+F4', u'Afslutter program')
        self.Bind(wx.EVT_MENU, self.on_quit, file_item_quit)     
        
        help_item_help = help_menu.Append(wx.ID_HELP, u'Hjælp\tF1')

        help_item_update = help_menu.Append(wx.ID_ANY, u'Check efter updatering\tCtrl+Alt+U')
        help_menu.Bind(wx.EVT_MENU, self.update_checker, help_item_update, wx.ID_ANY)

        help_item_about = help_menu.Append(wx.ID_ABOUT, u'Om\tCtrl+Alt+A')
        help_menu.Bind(wx.EVT_MENU, self.about_dialog, help_item_about, wx.ID_ANY)
        
        menubar.Append(file_menu, u'&Filer')
        menubar.Append(edit_menu, u'&Rediger')
#        menubar.Append(view_menu, u'&Vis')
        menubar.Append(help_menu, u'&Hjælp')
        
        self.SetMenuBar(menubar)
        
        self.Bind(wx.EVT_CLOSE, self.on_quit)

        # Tray icon
        # self.TrayIcon = tbi.Icon(self, wx.Icon('resources/icon/book.ico', wx.BITMAP_TYPE_ICO), "ToolTip Help Text Here")

        app_icon = wx.Icon('resources/icon/book.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(app_icon)

        #=======================================================================
        # Creates toolbar 
        #=======================================================================
        toolbar = wx.ToolBar(self, 0, style=wx.HORIZONTAL | wx.NO_BORDER)
        
        # Adds add book button to toolbar --------------------------------------
        add_icon = wx.Image('resources/icon/plus.png', wx.BITMAP_TYPE_PNG) 
#        add_icon_scale = add_icon.Scale(40, 40)
        add_icon_bitmap = add_icon.ConvertToBitmap()
        toolbar_add = toolbar.AddSimpleTool(1, add_icon_bitmap , u'Tilføj ', u'Tilføj ')   
        self.Bind(wx.EVT_TOOL, lambda event: self.add_dialog(event, None), toolbar_add)
        
        toolbar.AddSeparator()
        
        # Adds remove book buttom to toolbar -----------------------------------
        delete_icon = wx.Image('resources/icon/basket.png', wx.BITMAP_TYPE_PNG)
#        delete_icon_scale = delete_icon.Scale(40, 40)
        delete_icon_bitmap = delete_icon.ConvertToBitmap()
        toolbar_remove = toolbar.AddSimpleTool(2, delete_icon_bitmap, u'Slet', u'Slet')
        self.Bind(wx.EVT_TOOL, self.delete_dialog, toolbar_remove)
        
        toolbar.AddSeparator()
        
        # Adds edit button to toolbar ------------------------------------------
        edit_icon = wx.Image('resources/icon/edit.png', wx.BITMAP_TYPE_PNG)
#        edit_icon_scale = edit_icon.Scale(40, 40)
        edit_icon_bitmap = edit_icon.ConvertToBitmap()
        toolbar_edit = toolbar.AddSimpleTool(3, edit_icon_bitmap, u'Rediger', u'Rediger')
        self.Bind(wx.EVT_TOOL, self.edit_item, toolbar_edit)
        
        toolbar.AddStretchableSpace()
        
        # search_ctrl = wx.SearchCtrl(toolbar, -1, size=(250, 25), style = wx.TE_PROCESS_ENTER)
        # search_ctrl.ShowCancelButton(True)
        # search_ctrl.SetDescriptiveText(u'Søg')
        # toolbar.InsertControl(6, search_ctrl)

        toolbar.Realize()
        self.SetToolBar(toolbar)

        # search_ctrl.Bind(wx.EVT_KEY_UP, self.search, wx.ID_ANY)
#  -----------------------------------------------------------------------------

        # Creates statusbar ----------------------------------------------------
        self.sb = self.CreateStatusBar(3)
        self.sb.SetStatusWidths([-6, -4, -2])
        
#        self.SetMaxSize(self.window_size)
        self.SetMinSize((520, 0))
        self.Centre()
        
        # Creates notebook view ------------------------------------------------
        self.p = wx.ScrolledWindow(self, -1)
        self.p.SetScrollbars(1, 1, 1, 1)

        self.nb = wx.Notebook(self.p, -1)
        # Adds pages to notebook view ------------------------------------------
        self.book_tab = books.Books_tab(self.nb)
        self.nb.AddPage(self.book_tab, u'Bøger', select=self.book_tab_selected)
        
        self.video_tab = Video_tab(self.nb)
        self.nb.AddPage(self.video_tab, u'Video', select=self.video_tab_selected)
        
        self.music_tab = Musik_tab(self.nb)
        self.nb.AddPage(self.music_tab, u'Musik', select=self.music_tab_selected)

        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.p.SetSizer(sizer) 
        
        self.nb_tab_name = self.nb.GetName()

        self.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGED, self.on_size)
        self.nb.Bind(wx.EVT_SIZE, self.music_tab.track_list_ctrl_size)
        self.nb.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.taskbar)
        self.tastbar_update_check()
        self.update()

    #===========================================================================
    # Dialog for creating a new book
    #===========================================================================
    def add_dialog(self, event, item):
        tab = self.nb.GetSelection()

        if item == None:
            tab = self.nb.GetSelection()
        else:
            tab = item
        
        if tab == 0:
            dialog = Add_book_dialog(self, u'Tilføj bog')
            dialog.ShowModal()
            
        elif tab == 1:
            dialog = Add_video_dialog(self, u'Tilføj video')
            dialog.ShowModal()
               
        elif tab == 2:
            dialog = Add_music_dialog(self, u'Tilføj musik', wx.ID_ANY)
            dialog.ShowModal()

    #===========================================================================
    # Dialog for editing a chosen book 
    #===========================================================================
    def edit_item(self, event):
        list_ctrl = wx.ListCtrl

        tab = self.nb.GetSelection()
        if tab == 0:
            list_ctrl = self.book_tab.list_ctrl
            selected = list_ctrl.GetFirstSelected()
            if list_ctrl.GetFirstSelected() != -1:
                dialog = Edit_book_dialog(self, u'Rediger bog', list_ctrl.GetItem(list_ctrl.GetFirstSelected()).GetText())
                dialog.ShowModal()
                list_ctrl.Select(selected)

            else:
                self.message_box(u'Vælg et element først!')
        elif tab == 1:
            list_ctrl = self.video_tab.list_ctrl
            
            if list_ctrl.GetFirstSelected() != -1:
                dialog = Edit_video_dialog(self, u'Rediger video', list_ctrl.GetItem(list_ctrl.GetFirstSelected()).GetText())
                dialog.ShowModal()   
            
            else:
                self.message_box(u'Vælg et element først!')
        elif tab == 2:
            list_ctrl = self.music_tab.list_ctrl
            
            if list_ctrl.GetFirstSelected() != -1:  
                dialog = Edit_music_dialog(self, u'Rediger musik', list_ctrl.GetItem(list_ctrl.GetFirstSelected()).GetText())
                dialog.ShowModal()    
            
            else:
                self.message_box(u'Vælg et element først!')
    #===========================================================================
    # Dialog for performing a search
    #===========================================================================
    def search(self, event):
        pass
    
    #===========================================================================
    # Adds the selected item to a list
    #===========================================================================
    def get_selected_items(self):
        
        selected_item_ids = []
        selected_tab = self.nb.GetSelection()
        listctrl = wx.ListCtrl
        
        if selected_tab == 0:
            listctrl = self.book_tab.list_ctrl
        
        elif selected_tab == 1:
            listctrl = self.video_tab.list_ctrl
        
        elif selected_tab == 2:
            listctrl = self.music_tab.list_ctrl
        
        i =  listctrl.GetFirstSelected()
        
        while len(selected_item_ids) != listctrl.GetSelectedItemCount():
            selected_item_ids.append(listctrl.GetItem(i).GetText())
            i = listctrl.GetNextSelected(i)
            
        return selected_item_ids
    
    #===========================================================================
    # Function that handels the right click on the table. 
    #===========================================================================
    def list_right_click_menu(self, event):    

        menu = wx.Menu()
        delete_item = menu.Append(-1, u'Slet\tCtrl+D')
        edit_item = menu.Append(-1, u'Rediger\tCtrl+E')

        self.Bind(wx.EVT_MENU, self.delete_dialog, delete_item, wx.ID_ANY)
        self.Bind(wx.EVT_MENU, self.edit_item, edit_item, wx.ID_ANY)
        
        self.PopupMenu(menu)
        
    #===========================================================================
    # Dialog that ask the user if he wants to delete the item or not
    #===========================================================================
    def delete_dialog(self, event): 
        text = u'Er du sikker på du vil slette?'
        selected_item_ids = self.get_selected_items()
        
        if len(selected_item_ids) != 0:
            
            deletedialog = wx.MessageDialog(self, text, u'Info', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
            result = deletedialog.ShowModal()
            
            if result == wx.ID_YES:
                
                self.delete_item(selected_item_ids)

                
            if result == wx.ID_NO:
                deletedialog.Destroy()
            
        else:
            self.message_box(u'Vælg et element først!')

    #===========================================================================
    # Deletes the item
    #===========================================================================
    def delete_item(self, ids):
        
        selected_tab = self.nb.GetSelection()
        
        if selected_tab == 0:
            Service.delete_item('Books', ids)
            self.book_tab.clear_text_fields()
        
        elif selected_tab == 1:
            Service.delete_item('Videos', ids)
            self.video_tab.clear_text_fields()
        
        elif selected_tab == 2:
            Service.delete_item('Music', ids)
            self.music_tab.clear_text_fields()
            
        self.update()

    #===========================================================================
    # Shows an info message box 
    #===========================================================================
    @staticmethod    
    def message_box(e):
        wx.MessageBox(e, u'Info', wx.OK | wx.ICON_INFORMATION)

    #===========================================================================
    # updates the data in the table
    #===========================================================================
    def update(self):

        self.video_tab.update_list('title')
        self.book_tab.update_list('title')
        self.music_tab.update_list('title')
        book_count = Service.get_book_count()
        self.taskbar(None)
        # self.book_tab.set_image(self.image)

    def taskbar(self, event):
        
        selected_tab = self.nb.GetSelection()
        
        if selected_tab == 0:
            self.sb.SetStatusText(u'Antal bøger ialt: ' + unicode(Service.get_book_count()), 2)
            
        if selected_tab == 1:
            self.sb.SetStatusText(u'Antal video ialt: ' + unicode(Service.get_video_count()), 2)
        
        if selected_tab == 2:
            self.sb.SetStatusText(u'Antal musik ialt: ' + unicode(Service.get_musik_count()), 2)

    def tastbar_update_check(self):

        r = Service.update_checker()
        if r != requests.ConnectionError:

            result = unicode(r.status_code)

            if result == u'200':
                version = unicode(r.text.split('"')[1])
                current_version = Service.get_current_version()

                if version != current_version:
                    self.sb.SetStatusText(u"Opdatering fundet " + version, 1)

    #===========================================================================
    # Quits the program    
    #===========================================================================
    def on_quit(self, e):
        
        tab = self.nb.GetSelection()
        Service.set_selected_tab(tab)
#        Service.set_bool_setting('program', 'is_first_use', False)
        self.Destroy()
        # self.TrayIcon.Destroy()

    #===========================================================================
    # Shows data from the chosen book in the text boxes
    #===========================================================================
    def get_items(self, event):

        if self.nb.GetSelection() == 0:
            book_id = self.book_tab.list_ctrl.GetItem(event.m_itemIndex).GetText()
            self.book_tab.get_items(book_id)

        if self.nb.GetSelection() == 1:
            video_id = self.video_tab.list_ctrl.GetItem(event.m_itemIndex).GetText()
            self.video_tab.get_items(video_id)

        if self.nb.GetSelection() == 2:
            music_id = self.music_tab.list_ctrl.GetItem(event.m_itemIndex).GetText()
            self.music_tab.get_items(music_id)
            
    def on_size(self, event):

        x = (self.book_tab.list_ctrl.GetSize().width - 165.1) * 0.50

        self.music_tab.tracks_list_ctrl.SetColumnWidth(2, x)
        self.music_tab.tracks_list_ctrl.SetColumnWidth(3, x)

        self.music_tab.list_ctrl.SetColumnWidth(1, x)
        self.music_tab.list_ctrl.SetColumnWidth(2, x)

        self.book_tab.list_ctrl.SetColumnWidth(1, x)
        self.book_tab.list_ctrl.SetColumnWidth(2, x)
        
        self.video_tab.list_ctrl.SetColumnWidth(1, x)
        self.video_tab.list_ctrl.SetColumnWidth(2, x)

        event.Skip()

    def music_track_size(self, event):
        x = (self.music_tab.tracks_list_ctrl.GetSize().width - 121) * 0.50
        self.music_tab.tracks_list_ctrl.SetColumnWidth(1, x)
        self.music_tab.tracks_list_ctrl.SetColumnWidth(2, x)
        event.Skip()

    def no_update_dialog(self, text):

        dialog = wx.MessageDialog(self, text, u'Info', wx.OK | wx.ICON_INFORMATION)

        result = dialog.ShowModal()

        if result == wx.ID_OK:
            dialog.Destroy()

    def update_dialog(self, text):
        link = "http://mrbsoft.dk/collector/download"
        dialog = wx.MessageDialog(self, text, u'Info', wx.YES | wx.NO | wx.ICON_INFORMATION)

        result = dialog.ShowModal()

        if result == wx.ID_YES:
            try:
                os.startfile(link)

            except Exception, e:
                print e

        if result == wx.ID_NO:
            dialog.Destroy()

    def update_checker(self, event):
        r = Service.update_checker()
        if r != requests.ConnectionError:

            result = unicode(r.status_code)
            if result == u'200':
                version = unicode(r.text.split('"')[1])
                current_version = Service.get_current_version()

                if version == current_version:

                    text = u'Ingen opdateringer! Du har den nyeste version af Collector'
                    self.no_update_dialog(text)

                if version != current_version:
                    text = u'Der er en opdatering tilgængelig \nVil du gå til download siden?'
                    self.update_dialog(text)

        else:
            text = u'Der er ingen internet forbindelse!'
            self.no_update_dialog(text)

    def about_dialog(self, event):
        text = ''''''
        info = wx.AboutDialogInfo()
        info.SetName("Collector")
        info.SetVersion(Service.get_current_version())
        info.SetDescription(text)
        info.SetCopyright("(C) 2013 info@mrbsoft.dk")

        wx.AboutBox(info)

