'''
Created on 01/03/2012

@author: Martin
'''
import wx

class Drag_and_drop(wx.FileDropTarget):
    target = 'Book.jpg'
    
    def __init__(self, panel, target):
        wx.FileDropTarget.__init__(self)
        self.target = target
        self.panel = panel
    
    #===========================================================================
    # Takes the file path og the new image  
    #===========================================================================
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.target = str(file)
            self.panel.book_image = self.target
            self.panel.convert_image(self.panel.book_image)
        
        