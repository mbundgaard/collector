#!/usr/bin/pythond
# -*- coding: utf-8 -*-h

'''
Created on 22/01/2012

@author: Martin
'''
import platform
import wx

# from gui.main_frame import Main_frame
from gui.books_tab import Books_tab
from gui.main_frame import Main_frame
from service.service import Service


def main():
                
    os = platform.platform()
    os_name = os.split('-')[0].lower()
     
    if os_name == 'linux':
        Main_frame.splitter_size = 485
        Books_tab.border_image = 4
        Books_tab.border_text = 1
        
    if os_name == 'windows':
        Main_frame.splitter_size = 450
        Books_tab.border_image = 7
        Books_tab.border_text = 7
        Main_frame.window_size = (1150, 620)
        
    if os_name == 'darwin':
        Main_frame.splitter_size = 485
        Books_tab.border_image = 4
        Books_tab.border_text = 1

    Main_frame.book_tab_selected = Service.get_bool_setting('tab', 'book_tab')
    Main_frame.video_tab_selected = Service.get_bool_setting('tab', 'video_tab')
    Main_frame.music_tab_selected = Service.get_bool_setting('tab', 'music_tab')

    app = wx.App()
    # app.RedirectStdio(filename=Service.get_home_path() + "/Log.txt")
    Main_frame().Show()
    app.MainLoop()

if __name__ == '__main__':
    main()